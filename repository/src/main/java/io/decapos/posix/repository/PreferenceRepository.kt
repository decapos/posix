package io.decapos.posix.repository

import android.content.SharedPreferences
import io.decapos.posix.shared.utils.OpenForTesting
import io.decapos.posix.shared.utils.SharedPrefLiveData
import javax.inject.Inject

// TODO: This class might be better to be split into each own repo, eg. getDefaultPrinter moved
//  to printerRepository, getCompany etc to a new SettingsRepository.
@OpenForTesting
class PreferenceRepository @Inject constructor(
    private val sharedPreference: SharedPreferences
) {
    fun getCompanyName() = sharedPreference.getString("company_name", "") ?: ""

    fun getCompanyAddress() = sharedPreference.getString("company_address", "") ?: ""

    fun getCompanyPhone() = sharedPreference.getString("company_phone", "") ?: ""

    fun getNotes() = sharedPreference.getString("notes", "") ?: ""

    fun setDefaultPrinterAddress(address: String) {
        sharedPreference.edit()
            .putString("default_printer", address)
            .apply()
    }

    fun getDefaultPrinterAddress() = sharedPreference.getString("default_printer", "") ?: ""

    fun getDefaultPrinterLiveData() =
        SharedPrefLiveData(sharedPreference, "default_printer", "")
}