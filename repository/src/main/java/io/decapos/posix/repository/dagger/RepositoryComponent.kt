package io.decapos.posix.repository.dagger

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import io.decapos.posix.database.dagger.DatabaseModule
import io.decapos.posix.remote.dagger.NetworkModule
import io.decapos.posix.repository.CartRepository
import io.decapos.posix.repository.FeedbackRepository
import io.decapos.posix.repository.GeneralRepository
import io.decapos.posix.repository.OrderRepository
import io.decapos.posix.repository.PreferenceRepository
import io.decapos.posix.repository.PrinterRepository
import io.decapos.posix.repository.ProductRepository
import okhttp3.Interceptor
import javax.inject.Singleton

@Component(
    modules = [
        DatabaseModule::class,
        NetworkModule::class
    ]
)
@Singleton
interface RepositoryComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun networkInterceptor(interceptor: Interceptor): Builder

        fun build(): RepositoryComponent
    }

    val cartRepository: CartRepository
    val productRepository: ProductRepository
    val orderRepository: OrderRepository
    val feedbackRepository: FeedbackRepository
    val preferenceRepository: PreferenceRepository
    val printerRepository: PrinterRepository
    val generalRepository: GeneralRepository
}