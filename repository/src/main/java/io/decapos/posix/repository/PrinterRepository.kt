package io.decapos.posix.repository

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import androidx.lifecycle.LiveData
import io.decapos.posix.database.dao.PrinterDao
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.OpenForTesting
import io.decapos.posix.vo.Printer
import javax.inject.Inject

@OpenForTesting
class PrinterRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val printerDao: PrinterDao
) {
    fun getUserPrinters(): LiveData<List<Printer>> {
        return printerDao.getAllPrinter()
    }

    fun insertToDb(name: String, address: String) {
        val item = Printer()
        item.name = name
        item.address = address

        appExecutors.diskIO().execute {
            printerDao.insertPrinter(item)
        }
    }

    fun getPairedPrinter(): Set<BluetoothDevice> {
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return mBluetoothAdapter?.bondedDevices ?: setOf()
    }

    suspend fun findPrinterByAddress(address: String) = printerDao.findPrinterByAddress(address)

    fun deletePrinterById(id: Int) {
        appExecutors.diskIO().execute {
            printerDao.deletePrinterById(id)
        }
    }

    fun getDefaultPrinter(address: String?): BluetoothDevice? {
        if (address.isNullOrBlank()) return null
        val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        return mBluetoothAdapter?.getRemoteDevice(address)
    }
}