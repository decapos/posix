package io.decapos.posix.repository

import io.decapos.posix.database.dao.ProductDao
import io.decapos.posix.shared.utils.OpenForTesting
import io.decapos.posix.vo.Product
import javax.inject.Inject

/**
 * Abstraction for accessing Product data.
 */
@OpenForTesting
class ProductRepository @Inject constructor(private val productDao: ProductDao) {
    fun getAllProduct() = productDao.getAllProduct()

    fun loadProductsWithCartItemsByName(query: String) = productDao.loadProductsWithCartItemByName("%$query%")

    fun getProductById(id: Int) = productDao.findProductById(id)

    suspend fun insert(name: String, price: Long, imagePath: String) =
        productDao.insertProduct(Product(name = name, price = price, imagePath = imagePath))

    suspend fun insert(product: Product) = productDao.insertProduct(product)

    suspend fun updateNameById(id: Int, name: String) {
        productDao.updateNameById(id, name)
    }

    suspend fun updatePriceById(id: Int, price: Long) {
        productDao.updatePriceById(id, price)
    }

    suspend fun deleteProduct(id: Int) {
        productDao.deleteProductById(id)
    }

    suspend fun updateImageById(id: Int, imagePath: String) {
        productDao.updateImageById(id, imagePath)
    }
}
