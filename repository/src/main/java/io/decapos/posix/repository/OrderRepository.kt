package io.decapos.posix.repository

import androidx.lifecycle.Transformations
import io.decapos.posix.database.dao.OrderDao
import io.decapos.posix.shared.utils.OpenForTesting
import io.decapos.posix.vo.MonthlySummary
import io.decapos.posix.vo.Order
import io.decapos.posix.vo.OrdersWithOrderItems
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import javax.inject.Inject

/**
 * Abstraction for accessing Order data.
 */
@OpenForTesting
class OrderRepository @Inject constructor(
    private val orderDao: OrderDao
) {
    fun getAllOrder() = orderDao.getAllOrder()

    fun isOrderEmpty() = Transformations.map(orderDao.getOrderCount()) { it == 0 }

    fun getOrderWithOrderItems(id: Int) = orderDao.getOrderWithOrderItem(id)

    fun getOrderWithOrderItemsLiveData(id: Int) = orderDao.getOrderWithOrderItemLiveData(id)

    fun getMonthlySummary() = Transformations.map(getAllOrder()) { order ->
        order.groupBy {
            val zonedDate = it.createdAt.atZone(ZoneId.systemDefault())
            YearMonth.from(zonedDate)
        }.map {
            MonthlySummary(it.value, it.key)
        }
    }

    fun getAllOrderWithOrderItemsLiveData() = orderDao.getAllOrderWithOrderItemsLiveData()

    fun getAllProductTotalOrdered() = orderDao.getAllProductTotalOrdered()

    suspend fun getAllOrderWithOrderItems() = orderDao.getAllOrderWithOrderItems()

    suspend fun deleteOrder(idOrder: Int) {
        deletedOrderBackup = orderDao.getOrderWithOrderItem(idOrder)
        orderDao.deleteOrderById(idOrder)
    }

    suspend fun undoLastDelete() = deletedOrderBackup?.let {
        orderDao.insertOrderWithOrderItems(it)
        deletedOrderBackup = null
    }

    suspend fun updateOrder(newOrder: Order) = orderDao.updateOrder(newOrder)

    private var deletedOrderBackup: OrdersWithOrderItems? = null
}
