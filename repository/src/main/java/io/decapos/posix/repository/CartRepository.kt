package io.decapos.posix.repository

import io.decapos.posix.database.dao.CartItemDao
import io.decapos.posix.database.dao.OrderDao
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.vo.CartItem
import io.decapos.posix.vo.CartItemsWithProduct
import io.decapos.posix.vo.Order
import io.decapos.posix.vo.OrderItem
import io.decapos.posix.vo.OrdersWithOrderItems
import io.decapos.posix.vo.Product
import javax.inject.Inject

class CartRepository @Inject constructor(
    private val orderDao: OrderDao,
    private val cartItemDao: CartItemDao,
    private val executor: AppExecutors
) {
    private var deletedCartItemBackup: List<CartItem>? = null

    fun insertProductToCart(product: Product) {
        val item = CartItem(null, product.idProduct ?: -1, 1)
        executor.diskIO().execute {
            cartItemDao.insertItem(item)
        }
    }

    fun insertCartItem(cartItems: List<CartItem>) {
        executor.diskIO().execute {
            cartItemDao.insertItem(cartItems)
        }
    }

    fun loadCartItemsWithProducts() = cartItemDao.loadCartItemsWithProducts()

    fun updateCartItem(item: CartItem) {
        executor.diskIO().execute {
            if (item.quantity == 0) cartItemDao.deleteCartItem(item)
            else cartItemDao.updateItem(item)
        }
    }

    // creates new order, move products from cart into orderItem, and clears cartItem
    suspend fun processOrder(
        cartItems: List<CartItemsWithProduct>,
        totalPayment: Long,
        totalPrice: Long
    ): Int {
        val order = Order(actualPayAmount = totalPrice, totalPayAmount = totalPayment)
        val orderItem = cartItems.map {
            val product = it.product.first()
            OrderItem(
                quantity = it.cartDetail.quantity,
                price = product.price,
                name = product.name
            )
        }
        val ordersWithOrderItems = OrdersWithOrderItems(order, orderItem)
        val orderId = orderDao.insertOrderWithOrderItems(ordersWithOrderItems)
        cartItemDao.clearCartItems()

        return orderId
    }

    suspend fun clearCartItems() {
        deletedCartItemBackup = cartItemDao.getCartItems()
        cartItemDao.clearCartItems()
    }

    fun restoreCartItems() {
        executor.diskIO().execute {
            deletedCartItemBackup?.let {
                cartItemDao.insertItem(it)
                deletedCartItemBackup = null
            }
        }
    }
}
