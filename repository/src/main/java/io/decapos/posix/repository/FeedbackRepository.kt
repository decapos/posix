package io.decapos.posix.repository

import io.decapos.posix.remote.service.FeedbackService
import io.decapos.posix.shared.utils.OpenForTesting
import javax.inject.Inject

@OpenForTesting
class FeedbackRepository @Inject constructor(
    private val feedbackService: FeedbackService
) {
    fun sendFeedback(message: String) =
        feedbackService.postFeedback(FeedbackService.PostMessagePayload(message))
}