package io.decapos.posix.repository

import io.decapos.posix.database.dao.UserDao

/**
 * Abstraction for accessing User data.
 */
class UserRepository constructor (private val userDao: UserDao) {

}