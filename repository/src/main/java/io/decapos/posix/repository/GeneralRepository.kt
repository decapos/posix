package io.decapos.posix.repository

import io.decapos.posix.database.PosixDb
import javax.inject.Inject

// This is a repository that does database-wide operations
// such as clear all data.
// TODO: there might be a better place to place this instead of as a repository,
//  its currently only used for clearing data during test
class GeneralRepository @Inject constructor(
    private val db: PosixDb
) {
    fun clearAllData() {
        db.apply {
            clearAllTables()
        }
    }
}