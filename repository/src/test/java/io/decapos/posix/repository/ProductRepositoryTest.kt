package io.decapos.posix.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import io.decapos.posix.database.PosixDb
import io.decapos.posix.vo.Product
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@ExperimentalCoroutinesApi
class ProductRepositoryTest {
    private lateinit var database: PosixDb

    private lateinit var repo: ProductRepository

    // needed to make LiveData updates synchronous
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            getApplicationContext(),
            PosixDb::class.java
        ).allowMainThreadQueries().build()

        repo = ProductRepository(database.productDao())
    }

    @After
    fun cleanUp() {
        database.close()
    }

    @Test
    fun insertProduct_retrieveProduct_success() = runBlocking {
        val product = Product(999, "test", 1000)

        repo.insert(product)
        val result = repo.getProductById(999)

        result.observeForever {  }
        assertThat(result.value).isEqualTo(product)
    }
}

fun LiveData<Any>.observeOnce() {
}