package io.decapos.posix.remote.dagger

import dagger.Module
import dagger.Provides
import io.decapos.posix.remote.service.FeedbackService
import okhttp3.Interceptor

@Module
class NetworkModule {

    @Provides
    fun provideFeedbackService(interceptor: Interceptor): FeedbackService = FeedbackService.create(interceptor)
}