package io.decapos.posix.remote.service

import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface FeedbackService {
    @POST("/postMessage")
    fun postFeedback(@Body feedback: PostMessagePayload): Call<ResponseBody>

    data class PostMessagePayload(val message: String)

    companion object {
        private const val BASE_URL = "https://us-central1-pos-9-ac83f.cloudfunctions.net"

        fun create(networkInterceptor: Interceptor): FeedbackService =
            create(networkInterceptor, HttpUrl.parse(BASE_URL)!!)

        fun create(networkInterceptor: Interceptor, httpUrl: HttpUrl): FeedbackService {
            val client = OkHttpClient.Builder()
                .addNetworkInterceptor(networkInterceptor)
                .build()

            return Retrofit.Builder()
                .baseUrl(httpUrl)
                .addConverterFactory(MoshiConverterFactory.create())
                .client(client)
                .build()
                .create(FeedbackService::class.java)
        }
    }
}