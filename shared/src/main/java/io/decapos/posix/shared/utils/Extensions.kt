package io.decapos.posix.shared.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView

fun BottomNavigationView.setSelectedItemId(itemId: Int, triggerAction: Boolean) {
    if (triggerAction) this.selectedItemId = itemId
    else this.menu.findItem(itemId).isChecked = true
}

inline fun <T> LiveData<T>.observe(lifecycleOwner: LifecycleOwner, crossinline callback: (T) -> Unit): Observer<T> {
    val observer = Observer<T> { callback(it) }
    this.observe(lifecycleOwner, observer)
    return observer
}

fun Fragment.hideKeyboard() {
    val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view?.windowToken, 0)
}