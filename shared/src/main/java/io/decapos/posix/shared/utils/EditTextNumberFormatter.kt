package io.decapos.posix.shared.utils

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.google.android.material.textfield.TextInputEditText
import java.text.ParseException

class EditTextNumberFormatter(
    owner: LifecycleOwner,
    private val editText: TextInputEditText,
    private val formatter: (String) -> String
) : TextWatcher, Runnable, LifecycleObserver {
    private var lastCursorLocation = 0
    private var lengthBefore = 0
    private var textBefore = ""
    private var lengthAfter = 0
    private var isKeyDelete = false
    private var delay: Long = 200
    private var lastEdit: Long = 0
    private var handler = Handler()

    init {
        // This is triggered before registered TextWatcherDetect is called, so we record
        // isKeyDelete here so we can know whether text change was triggered by deleteOrder
        // key or not
        editText.setOnKeyListener { _, keyCode, event ->
            isKeyDelete = keyCode == KeyEvent.KEYCODE_DEL
            if (event!!.action == KeyEvent.ACTION_UP) {
                handler.postDelayed(this, delay)
            }
            false
        }
        editText.addTextChangedListener(this)
        owner.lifecycle.addObserver(this)
    }

    // Updates text value only if `delay` amount of time has passed since
    // last text changed, so that holding delete button works.
    override fun run() {
        if (System.currentTimeMillis() > lastEdit + delay && isKeyDelete) {
            val value = editText.text.toString()
            val formattedText = if (value == "") formatter("0") else formatter(value)
            if (formattedText != value) {
                editText.setText(formattedText)
            }
        }
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        lastCursorLocation = editText.selectionStart
        lengthBefore = s?.length ?: 0
        textBefore = s.toString()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        handler.removeCallbacks(this, delay)
        if (s.isNullOrEmpty()) return

        // Figure out where cursor should be placed next
        lengthAfter = s.length
        val selectionOffset = lengthAfter - lengthBefore
        editText.setSelection(lastCursorLocation + selectionOffset)
        lastCursorLocation = editText.selectionStart

        // Reformat text unless text change was triggered by deleteOrder key
        lastEdit = System.currentTimeMillis()
        if (isKeyDelete && s.isNotEmpty()) return
        // TODO: this repeats with the run block, why? Can it be simplified?
        val formattedText = formatter(s.toString())
        if (formattedText != textBefore) {
            editText.setText(formattedText)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unregister() {
        editText.removeTextChangedListener(this)
    }

    override fun afterTextChanged(s: Editable?) {
        /* Nothing to do here */
    }
}

fun TextInputEditText.attachCurrencyFormatter(lifecycleOwner: LifecycleOwner): EditTextNumberFormatter {
    val formatter = { input: String ->
        val cleanedText = try {
            Converter.stringCurrencyToLong(input)
        } catch (error: ParseException) {
            input.toLong()
        }
        Converter.longToStringCurrency(cleanedText)
    }
    return EditTextNumberFormatter(lifecycleOwner, this, formatter)
}

fun TextInputEditText.attachPercentageFormatter(lifecycleOwner: LifecycleOwner): EditTextNumberFormatter {
    val formatter = { input: String ->
        when {
            input.isBlank() -> "0"
            input.toInt() > 100 -> "100"
            // removes zero prefixes
            else -> input.toInt().toString()
        }
    }
    return EditTextNumberFormatter(lifecycleOwner, this, formatter)
}
