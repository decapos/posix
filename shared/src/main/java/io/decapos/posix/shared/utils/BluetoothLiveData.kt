package io.decapos.posix.shared.utils

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.LiveData
import timber.log.Timber
import javax.inject.Inject

class BluetoothLiveData @Inject constructor(
    private val app: Application
) : LiveData<List<BluetoothDevice>>() {
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            // TODO: Can be replaced with if.
            val action: String = intent?.action ?: return
            when(action) {
                BluetoothDevice.ACTION_FOUND -> {
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    val device: BluetoothDevice =
                        intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    var deviceList = value?.toMutableList()
                    if (deviceList == null) {
                        deviceList = mutableListOf()
                    }
                    deviceList.add(device)
                    value = deviceList
                }
            }
        }
    }

    override fun onActive() {
        val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
        app.registerReceiver(receiver, filter)
        val isSuccess = BluetoothAdapter.getDefaultAdapter().startDiscovery()
        Timber.log(1, isSuccess.toString())
        super.onActive()
    }

    override fun onInactive() {
        val devices = BluetoothAdapter.getDefaultAdapter()
        value = mutableListOf()
        app.unregisterReceiver(receiver)
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
        super.onInactive()
    }
}