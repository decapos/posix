package io.decapos.posix.shared.utils

import android.content.SharedPreferences
import androidx.lifecycle.LiveData

class SharedPrefLiveData(
    private val sharedPref: SharedPreferences,
    private val key: String,
    private val defaultValue: String
) : LiveData<String>() {

    val listener = SharedPreferences.OnSharedPreferenceChangeListener { sharedPreferences, changedKey ->
        if (changedKey == key) value = sharedPreferences.getString(key, defaultValue)
    }

    override fun onActive() {
        value = sharedPref.getString(key, defaultValue)
        sharedPref.registerOnSharedPreferenceChangeListener(listener)
    }

    override fun onInactive() {
        sharedPref.unregisterOnSharedPreferenceChangeListener(listener)
    }
}