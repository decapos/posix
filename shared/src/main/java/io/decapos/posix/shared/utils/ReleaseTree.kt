package io.decapos.posix.shared.utils

import com.bugsnag.android.Bugsnag
import timber.log.Timber

/**
 * Timber tree that is used on production to log to bugsnag
 * instead of to console (DebugTree is used for debug)
 *
 * */
class ReleaseTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        Bugsnag.leaveBreadcrumb("$tag: $message")
    }
}