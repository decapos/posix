package io.decapos.posix.shared.utils

import android.app.Application
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.lifecycle.LiveData
import timber.log.Timber
import javax.inject.Inject

class BluetoothBondStateLiveData @Inject constructor(
    private val app: Application
) : LiveData<Int>() {
    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            value = intent?.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.ERROR)
        }
    }

    override fun onActive() {
        val filter = IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED)
        app.registerReceiver(receiver, filter)
        val isSuccess = BluetoothAdapter.getDefaultAdapter().startDiscovery()
        Timber.log(1, isSuccess.toString())
        super.onActive()
    }

    override fun onInactive() {
        value = BluetoothDevice.BOND_NONE
        app.unregisterReceiver(receiver)
        BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
        super.onInactive()
    }
}