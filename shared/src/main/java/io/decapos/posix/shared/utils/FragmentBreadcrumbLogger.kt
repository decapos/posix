package io.decapos.posix.shared.utils

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.bugsnag.android.BreadcrumbType
import com.bugsnag.android.Bugsnag

class FragmentBreadcrumbLogger : FragmentManager.FragmentLifecycleCallbacks() {

    override fun onFragmentViewCreated(fm: FragmentManager, f: Fragment, v: View, savedInstanceState: Bundle?) {
        leaveLifecycleBreadcrumb(f, "onFragmentViewCreated()")
    }

    override fun onFragmentViewDestroyed(fm: FragmentManager, f: Fragment) {
        leaveLifecycleBreadcrumb(f, "onFragmentViewDestroyed()")
    }

    // leave Breadcrumbs in other lifecycle callbacks if needed
    private fun leaveLifecycleBreadcrumb(fragment: Fragment, lifecycleCallback: String) {
        // note - if proguard is enabled you may want to use a different method of obtaining
        // the current fragment's name
        val fragmentName = fragment.javaClass.simpleName

        val metadata = HashMap<String, String>()
        metadata["FragmentLifecycleCallback"] = lifecycleCallback
        Bugsnag.leaveBreadcrumb(fragmentName, BreadcrumbType.NAVIGATION, metadata)
    }
}
