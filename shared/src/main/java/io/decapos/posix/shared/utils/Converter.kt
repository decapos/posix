package io.decapos.posix.shared.utils

import org.threeten.bp.Instant
import org.threeten.bp.Month
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import org.threeten.bp.format.DateTimeFormatter
import org.threeten.bp.format.FormatStyle
import org.threeten.bp.format.TextStyle
import java.text.NumberFormat
import java.text.ParseException
import java.util.Locale

/**
 * Static class for converting values, used mostly for converting values to be displayed on the UI, ex Date object
 * into a formatted string.
 * */

object Converter {
    private val numberFormatter by lazy {
        NumberFormat.getNumberInstance(Locale("in", "ID"))
    }

    private val currencyFormatter = NumberFormat.getCurrencyInstance(Locale("in", "ID"))

    @JvmStatic
    fun instantToString(date: Instant): String = DateTimeFormatter
        .ofLocalizedDate(FormatStyle.FULL)
        .withLocale(Locale("ID"))
        .format(date.atZone(ZoneId.systemDefault()))

    @JvmStatic
    fun yearMonthToString(date: YearMonth) = """
        ${monthToString(date.month)} ${date.year}
    """.trimIndent()

    @JvmStatic
    fun monthToString(month: Month): String =
        month.getDisplayName(TextStyle.FULL, Locale.getDefault())

    @JvmStatic
    fun longToStringCurrency(amount: Long): String {
        currencyFormatter.maximumFractionDigits = 0
        return currencyFormatter.format(amount)
    }

    @JvmStatic
    fun stringCurrencyToLong(amount: String?): Long {
        if (amount.isNullOrBlank()) return 0
        if (amount == "Rp" || amount == "R") return 0
        currencyFormatter.maximumFractionDigits = 0
        return try {
            currencyFormatter
                .parse(amount)
                ?.toLong() ?: 0L
        } catch (error: ParseException) {
            amount.replace(Regex("[^0-9]"), "").toLong()
        }
    }

    @JvmStatic
    fun instantToStringDate(date: Instant): String = DateTimeFormatter
        .ofPattern("dd/mm/yyyy")
        .withLocale(Locale("ID"))
        .format(date.atZone(ZoneId.systemDefault()))

    @JvmStatic
    fun instantToStringTime(date: Instant): String = DateTimeFormatter
        .ofPattern("HH:mm")
        .withLocale(Locale("ID"))
        .format(date.atZone(ZoneId.systemDefault()))
}
