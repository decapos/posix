package io.decapos.posix.shared.utils

import android.text.TextWatcher
import android.view.View
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputEditText

object BindingAdapters {
    @JvmStatic
    @BindingAdapter("textChangedListener")
    fun bindTextWatcher(view: TextInputEditText, textWatcher: TextWatcher) {
        view.addTextChangedListener(textWatcher)
    }

    @JvmStatic
    @BindingAdapter("onKeyListener")
    fun bindKeyListener(view: TextInputEditText, listener: View.OnKeyListener) {
        view.setOnKeyListener(listener)
    }
}