# Deca Android Application
[![Build Status](https://app.bitrise.io/app/7268afad5ca237cf/status.svg?token=9pQNSV9rZ2UbdRbVl9IsaA)](https://app.bitrise.io/app/7268afad5ca237cf)
[![codecov](https://codecov.io/gl/decapos/posix/branch/develop/graph/badge.svg)](https://codecov.io/gl/decapos/posix)

An open source android based Point of Sale application focused on flexibility and extensibility.

## Community
### Spectrum
Join our [spectrum](https://spectrum.chat/deca) community to submit and vote feature request, report bugs and discuss ideas and things with us
and other users.


## Stability Monitoring
Deca uses [Bugsnag for Open Source](https://www.bugsnag.com/open-source/) for stability and application monitoring. Thx bugsnag :).


[![Bugnsag logo](https://gitlab.com/null-point/pos-9/raw/develop/images/bugsnag_logo_navy.svg)](https://bugsnag.com)
