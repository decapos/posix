package io.decapos.posix.database

import androidx.core.database.getIntOrNull
import androidx.core.database.getStringOrNull
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.platform.app.InstrumentationRegistry
import com.google.common.truth.Truth.assertThat
import io.decapos.posix.test_shared.TestUtils
import io.decapos.posix.vo.CartItem
import org.junit.Rule
import org.junit.Test
import java.io.IOException
import java.time.Instant

class MigrationTest {
    private val TEST_DB = "migration-test"

    @get:Rule
    val helper: MigrationTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        io.decapos.posix.database.PosixDb::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    @Throws(IOException::class)
    fun migrate4To5() {
        val name = "Draco"
        val price = 1000
        val path = "\\"
        val createdTime = Instant.now()
        val updatedAt = Instant.now()

        @Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")
        var db = helper.createDatabase(TEST_DB, 4).apply {
            // Fill db with data (this migration doesn't alter existing table so it doesn't need many mock data)
            execSQL("INSERT INTO product (name, price, imagePath, createdAt, updatedAt) VALUES ('$name', $price, '$path', '$createdTime', '$updatedAt')")
            close()
        }

        // run migration
        db = helper.runMigrationsAndValidate(TEST_DB, 5, true, Migration_4_5)

        // verify data integrity
        val cursor = db.query("SELECT * FROM product")
        cursor.moveToNext()
        assertThat(cursor.count).isEqualTo(1)
        assertThat(cursor.getString(1)).isEqualTo(name)
        assertThat(cursor.getLong(2)).isEqualTo(price)
        assertThat(cursor.getString(3)).isEqualTo(path)
        assertThat(cursor.getString(4)).isEqualTo(createdTime.toString())
        assertThat(cursor.getString(4)).isEqualTo(updatedAt.toString())
    }

    @Test
    @Throws(IOException::class)
    fun migrate5To6() {
        val name = "Draco"
        val price = 1000
        val path = "\\"
        val createdTime = Instant.now()
        val updatedAt = Instant.now()

        @Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")
        var db = helper.createDatabase(TEST_DB, 5).apply {
            // Fill db with data (this migration doesn't alter existing table so it doesn't need many mock data)
            execSQL("INSERT INTO product (name, price, imagePath, createdAt, updatedAt) VALUES ('$name', $price, '$path', '$createdTime', '$updatedAt')")
            close()
        }

        // run migration
        db = helper.runMigrationsAndValidate(TEST_DB, 6, true, Migration_5_6)

        // verify data integrity
        val cursor = db.query("SELECT * FROM product")
        cursor.moveToNext()
        assertThat(cursor.count).isEqualTo(1)
        assertThat(cursor.getString(1)).isEqualTo(name)
        assertThat(cursor.getLong(2)).isEqualTo(price)
        assertThat(cursor.getString(3)).isEqualTo(path)
        assertThat(cursor.getString(4)).isEqualTo(createdTime.toString())
        assertThat(cursor.getString(4)).isEqualTo(updatedAt.toString())
    }

    @Test
    @Throws(IOException::class)
    fun migrate6to7() {
        val name = "Draco"
        val price = 1000
        val path = "\\"
        val createdTime = Instant.now()
        val updatedAt = Instant.now()

        val order = TestUtils.createOrder()
        val orderItem = TestUtils.createOrderItem(order)
        val cartItem = CartItem(0, 0, 10)

        @Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")
        var db = helper.createDatabase(TEST_DB, 6).apply {
            // Fill db with data (this migration doesn't alter existing table so it doesn't need many mock data)
            execSQL("INSERT INTO `Product` (name, price, imagePath, createdAt, updatedAt) VALUES ('$name', $price, '$path', '$createdTime', '$updatedAt')")
            execSQL("INSERT INTO `Order` (idOrder, idCustomer, idUser, tax, totalPayAmount, actualPayAmount, createdAt, updatedAt, completed) VALUES (${order.idOrder}, NULL, NULL, ${order.tax},${order.totalPayAmount}, ${order.actualPayAmount},'${order.createdAt}','${order.updatedAt}', 'FALSE')")
            execSQL("INSERT INTO `OrderItem` (idOrderItem, idOrder, idCustomer, name, quantity, price, createdAt, updatedAt) VALUES (${orderItem.idOrderItem}, ${orderItem.idOrder}, NULL, '${orderItem.name}', ${orderItem.quantity}, ${orderItem.price}, '${orderItem.createdAt}', '${orderItem.updatedAt}')")
            execSQL("INSERT INTO `CartItem` (idCartItem, idProduct, quantity) VALUES (${cartItem.idCartItem}, ${cartItem.idProduct}, ${cartItem.quantity} )")
            close()
        }

        // run migration
        db = helper.runMigrationsAndValidate(TEST_DB, 7, true, Migration_6_7)

        // verify data integrity
        val productQuery = db.query("SELECT * FROM product")
        productQuery.moveToNext()
        assertThat(productQuery.count).isEqualTo(1)
        assertThat(productQuery.getString(1)).isEqualTo(name)
        assertThat(productQuery.getLong(2)).isEqualTo(price)
        assertThat(productQuery.getString(3)).isEqualTo(path)
        assertThat(productQuery.getString(4)).isEqualTo(createdTime.toString())
        assertThat(productQuery.getString(5)).isEqualTo(updatedAt.toString())

        val orderQuery = db.query("SELECT * FROM `Order`")
        orderQuery.moveToNext()
        assertThat(orderQuery.count).isEqualTo(1)
        assertThat(orderQuery.getInt(0)).isEqualTo(order.idOrder)
        assertThat(orderQuery.getIntOrNull(1)).isEqualTo(order.idCustomer)
        assertThat(orderQuery.getStringOrNull(2)).isEqualTo(order.idUser)
        assertThat(orderQuery.getStringOrNull(3)).isEqualTo(null)
        assertThat(orderQuery.getFloat(4)).isEqualTo(order.tax)
        assertThat(orderQuery.getLong(5)).isEqualTo(order.totalPayAmount)
        assertThat(orderQuery.getLong(6)).isEqualTo(order.actualPayAmount)
        assertThat(orderQuery.getString(7)).isEqualTo(order.createdAt.toString())
        assertThat(orderQuery.getString(8)).isEqualTo(order.updatedAt.toString())

        val orderItemQuery = db.query("SELECT * FROM OrderItem")
        orderItemQuery.moveToNext()
        assertThat(orderItemQuery.count).isEqualTo(1)
        assertThat(orderItemQuery.getInt(0)).isEqualTo(orderItem.idOrderItem)
        assertThat(orderItemQuery.getIntOrNull(1)).isEqualTo(orderItem.idOrder)
        assertThat(orderItemQuery.getString(2)).isEqualTo(orderItem.name)
        assertThat(orderItemQuery.getInt(3)).isEqualTo(orderItem.quantity)
        assertThat(orderItemQuery.getLong(4)).isEqualTo(orderItem.price)
        assertThat(orderItemQuery.getString(5)).isEqualTo(orderItem.createdAt.toString())
        assertThat(orderItemQuery.getString(6)).isEqualTo(orderItem.updatedAt.toString())

        val cartItemQuery = db.query("SELECT * FROM CartItem")
        cartItemQuery.moveToNext()
        assertThat(cartItemQuery.count).isEqualTo(1)
        assertThat(cartItemQuery.getInt(0)).isEqualTo(cartItem.idCartItem)
        assertThat(cartItemQuery.getInt(1)).isEqualTo(cartItem.idProduct)
        assertThat(cartItemQuery.getInt(2)).isEqualTo(cartItem.quantity)
    }
}