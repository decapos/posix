package io.decapos.posix.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import io.decapos.posix.test_shared.getValue
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class OrderDaoTest : io.decapos.posix.database.DbTest() {
    private lateinit var orderDao: io.decapos.posix.database.dao.OrderDao
    override fun setupDb() {
        orderDao = db.orderDao()
    }

    @Test
    fun insertAndRead() {
        val order = io.decapos.posix.test_shared.TestUtils.createOrder()
        orderDao.insertOrder(order)
        assertThat(getValue(orderDao.getAllOrder()).first()).isEqualTo(order)
    }
}