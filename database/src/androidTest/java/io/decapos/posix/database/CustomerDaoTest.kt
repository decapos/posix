package io.decapos.posix.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import io.decapos.posix.database.dao.CustomerDao
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CustomerDaoTest : DbTest() {
    private lateinit var customerDao: CustomerDao
    override fun setupDb() {
        customerDao = db.customerDao()
    }

    @Test
    @Throws(Exception::class)
    fun insertAndRead() {
        val customer = io.decapos.posix.test_shared.TestUtils.createCustomer("John")
        customerDao.insertCustomer(customer)
        assertThat(
            io.decapos.posix.test_shared.getValue(customerDao.getAllCustomer()).first()
        ).isEqualTo(customer)
    }

    @Test
    fun deleteById() {
        val customer = io.decapos.posix.test_shared.TestUtils.createCustomer("Nathan")
        customerDao.insertCustomer(customer)
        assertThat(io.decapos.posix.test_shared.getValue(customerDao.getAllCustomer()).size).isEqualTo(1)
        customerDao.deleteCustomerById(1)
        assertThat(io.decapos.posix.test_shared.getValue(customerDao.getAllCustomer()).size).isEqualTo(0)
    }
}