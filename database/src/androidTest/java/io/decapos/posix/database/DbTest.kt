package io.decapos.posix.database

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider.getApplicationContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import java.io.IOException

abstract class DbTest {
    lateinit var db: PosixDb

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val ctx = getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(ctx, PosixDb::class.java).build()
        setupDb()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    abstract fun setupDb()
}