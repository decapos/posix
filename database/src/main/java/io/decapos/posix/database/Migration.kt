package io.decapos.posix.database

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

val Migration_4_5 = object : Migration(4, 5) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE IF NOT EXISTS `CartItem` (`idCartItem` INTEGER, `idProduct` INTEGER NOT NULL, `quantity` INTEGER NOT NULL, PRIMARY KEY(`idCartItem`))")
    }
}

val Migration_5_6 = object : Migration(5, 6) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE IF NOT EXISTS `Printer` (`idPrinter` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `address` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL)")
    }
}

val Migration_6_7 = object : Migration(6, 7) {
    override fun migrate(database: SupportSQLiteDatabase) {
        // Create all new tables
        database.execSQL("CREATE TABLE IF NOT EXISTS `CustomLabelName` (`idCustomLabelName` INTEGER PRIMARY KEY AUTOINCREMENT, `name` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL)")
        database.execSQL("CREATE TABLE IF NOT EXISTS `CustomLabelValue` (`idCustomLabelValue` INTEGER PRIMARY KEY AUTOINCREMENT, `idCustomLabelName` INTEGER, `value` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, FOREIGN KEY(`idCustomLabelName`) REFERENCES `CustomLabelName`(`idCustomLabelName`) ON UPDATE CASCADE ON DELETE CASCADE )")
        database.execSQL("CREATE TABLE IF NOT EXISTS `OrderCustomLabel` (`idOrderCustomLabel` INTEGER PRIMARY KEY AUTOINCREMENT, `idCustomLabelValue` INTEGER, `idOrder` INTEGER, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, FOREIGN KEY(`idCustomLabelValue`) REFERENCES `CustomLabelValue`(`idCustomLabelValue`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`idOrder`) REFERENCES `Order`(`idOrder`) ON UPDATE CASCADE ON DELETE CASCADE )")
        database.execSQL("CREATE TABLE IF NOT EXISTS `OrderStatus` (`idStatus` INTEGER PRIMARY KEY AUTOINCREMENT, `name` INTEGER NOT NULL)")
        database.execSQL("CREATE TABLE IF NOT EXISTS `Tag` (`idTag` INTEGER PRIMARY KEY AUTOINCREMENT, `value` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL)")
        database.execSQL("CREATE TABLE IF NOT EXISTS `ProductTag` (`idProductTag` INTEGER PRIMARY KEY AUTOINCREMENT, `idTag` INTEGER NOT NULL, `idProduct` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, FOREIGN KEY(`idTag`) REFERENCES `Tag`(`idTag`) ON UPDATE CASCADE ON DELETE CASCADE , FOREIGN KEY(`idProduct`) REFERENCES `Product`(`idProduct`) ON UPDATE CASCADE ON DELETE CASCADE )")

        // Migrate Order table
        database.execSQL("CREATE TABLE IF NOT EXISTS `NewOrder` (`idOrder` INTEGER PRIMARY KEY AUTOINCREMENT, `idCustomer` INTEGER, `idUser` INTEGER, `idStatus` INTEGER, `tax` REAL NOT NULL, `totalPayAmount` INTEGER NOT NULL, `actualPayAmount` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, FOREIGN KEY(`idCustomer`) REFERENCES `Customer`(`idCustomer`) ON UPDATE CASCADE ON DELETE SET NULL , FOREIGN KEY(`idStatus`) REFERENCES `OrderStatus`(`idStatus`) ON UPDATE CASCADE ON DELETE SET NULL )")
        database.execSQL("INSERT INTO `NewOrder`(idOrder, idCustomer, idUser, idStatus, tax, TotalPayAmount, actualPayAmount, createdAt, updatedAt) SELECT idOrder, idCustomer, idUser, NULL, tax, TotalPayAmount, actualPayAmount, createdAt, updatedAt FROM `Order`")
        database.execSQL("DROP TABLE `Order`")
        database.execSQL("ALTER TABLE `NewOrder` RENAME TO `Order`")
        database.execSQL("CREATE INDEX `index_Order_idCustomer` ON `Order`(idCustomer)")

        // Migrate OrderItem table
        database.execSQL("CREATE TABLE IF NOT EXISTS `NewOrderItem` (`idOrderItem` INTEGER PRIMARY KEY AUTOINCREMENT, `idOrder` INTEGER, `name` TEXT NOT NULL, `quantity` INTEGER NOT NULL, `price` INTEGER NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, FOREIGN KEY(`idOrder`) REFERENCES `Order`(`idOrder`) ON UPDATE CASCADE ON DELETE CASCADE )")
        database.execSQL("INSERT INTO `NewOrderItem` (idOrderItem, idOrder, name, quantity, price, createdAt, updatedAt) SELECT idOrderItem, idOrder, name, quantity, price, createdAt, updatedAt FROM `OrderItem`")
        database.execSQL("DROP TABLE `OrderItem`")
        database.execSQL("ALTER TABLE `NewOrderItem` RENAME TO `OrderItem`")
        database.execSQL("CREATE  INDEX `index_OrderItem_idOrder` ON `OrderItem` (`idOrder`)")

        // Migrate Product table
        database.execSQL("CREATE TABLE IF NOT EXISTS `NewCartItem` (`idCartItem` INTEGER PRIMARY KEY AUTOINCREMENT, `idProduct` INTEGER, `quantity` INTEGER NOT NULL, FOREIGN KEY(`idProduct`) REFERENCES `Product`(`idProduct`) ON UPDATE CASCADE ON DELETE CASCADE )")
        database.execSQL("INSERT INTO `NewCartItem` SELECT * FROM CartItem")
        database.execSQL("DROP TABLE `CartItem`")
        database.execSQL("ALTER TABLE `NewCartItem` RENAME TO `CartItem`")
        database.execSQL("CREATE  INDEX `index_CartItem_idProduct` ON `CartItem` (`idProduct`)")

        // Create indexes
        database.execSQL("CREATE  INDEX `index_CustomLabelValue_idCustomLabelName` ON `CustomLabelValue` (`idCustomLabelName`)")
        database.execSQL("CREATE  INDEX `index_OrderCustomLabel_idCustomLabelValue` ON `OrderCustomLabel` (`idCustomLabelValue`)")
        database.execSQL("CREATE  INDEX `index_OrderCustomLabel_idOrder` ON `OrderCustomLabel` (`idOrder`)")
        database.execSQL("CREATE  INDEX `index_ProductTag_idProduct` ON `ProductTag` (`idProduct`)")
        database.execSQL("CREATE  INDEX `index_ProductTag_idTag` ON `ProductTag` (`idTag`)")
        database.execSQL("CREATE  INDEX `index_Order_idStatus` ON `Order` (`idStatus`)")
    }
}