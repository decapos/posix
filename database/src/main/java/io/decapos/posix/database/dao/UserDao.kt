package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import io.decapos.posix.vo.User

/** Used for querying User table from DB. */
@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Query("select * from user where name = :name")
    fun findUserByName(name: String): User?

    @Query("select * from user where idUser = :id")
    fun findUserById(id: Int): User?

    @Query("select * from user order by name asc")
    fun getAllUser(): LiveData<List<User>>

    @Query("select * from user where pin = :pin")
    fun loginByPin(pin: String): User?
}
