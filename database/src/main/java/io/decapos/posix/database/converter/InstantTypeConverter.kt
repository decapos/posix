package io.decapos.posix.database.converter

import androidx.room.TypeConverter
import org.threeten.bp.Instant

/** For converting date into long, used for saving into SQLite using Room. */
object InstantTypeConverter {
    /** Convert date from long type to Date (to get from DB and returned. */
    @TypeConverter
    @JvmStatic
    fun fromTimestamp(value: String) = Instant.parse(value)

    /** Convert date from Date type to Long (to save into DB). */
    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: Instant) = date.toString()
}
