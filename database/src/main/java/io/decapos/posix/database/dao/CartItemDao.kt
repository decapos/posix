package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import io.decapos.posix.vo.CartItem
import io.decapos.posix.vo.CartItemsWithProduct

@Dao
interface CartItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(cartItem: CartItem)

    @Transaction
    @Query("SELECT * FROM CartItem")
    fun loadCartItemsWithProducts(): LiveData<List<CartItemsWithProduct>>

    @Delete
    fun deleteCartItem(item: CartItem): Int

    @Update
    fun updateItem(item: CartItem): Int

    @Query("DELETE FROM CartItem")
    suspend fun clearCartItems(): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItem(cartItem: List<CartItem>)

    @Query("SELECT * FROM CartItem")
    fun getCartItems(): List<CartItem>

}