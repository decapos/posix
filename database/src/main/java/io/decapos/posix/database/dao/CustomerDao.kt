package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import io.decapos.posix.vo.Customer

/** Used for querying Customer table from DB. */
@Dao
interface CustomerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCustomer(customer: Customer): Long

    @Query("select * from customer where idCustomer = :id")
    fun findCustomerById(id: Int): Customer?

    @Query("select * from customer order by name asc")
    fun getAllCustomer(): LiveData<List<Customer>>

    @Query("delete from customer where idCustomer = :id")
    fun deleteCustomerById(id: Int): Int
}
