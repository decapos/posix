package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.decapos.posix.vo.Printer

@Dao
interface PrinterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPrinter(printer: Printer)

    @Query("select * from printer order by idPrinter asc")
    fun getAllPrinter(): LiveData<List<Printer>>

    @Query("update printer set name = :name where idPrinter = :id")
    fun updatePrinterName(name: String, id: Int): Int

    @Query("select * from printer where address = :address")
    suspend fun findPrinterByAddress(address: String): Printer?

    @Query("delete from printer where idPrinter = :id")
    fun deletePrinterById(id: Int): Int
}