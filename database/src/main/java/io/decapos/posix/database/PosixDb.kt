package io.decapos.posix.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import io.decapos.posix.vo.CartItem
import io.decapos.posix.vo.CustomLabelName
import io.decapos.posix.vo.CustomLabelValue
import io.decapos.posix.vo.Customer
import io.decapos.posix.vo.Order
import io.decapos.posix.vo.OrderCustomLabel
import io.decapos.posix.vo.OrderItem
import io.decapos.posix.vo.OrderStatus
import io.decapos.posix.vo.Printer
import io.decapos.posix.vo.Product
import io.decapos.posix.vo.ProductTag
import io.decapos.posix.vo.Tag
import io.decapos.posix.vo.User

/** RoomDatabase definition.
 *
 * Used for setting up Room for Posix.
 * */
@Database(
    entities = [
        Customer::class,
        Order::class,
        OrderItem::class,
        Printer::class,
        Product::class,
        User::class,
        CartItem::class,
        CustomLabelName::class,
        CustomLabelValue::class,
        OrderCustomLabel::class,
        OrderStatus::class,
        ProductTag::class,
        Tag::class
    ],
    version = 7
)
@TypeConverters(io.decapos.posix.database.converter.InstantTypeConverter::class)
abstract class PosixDb : RoomDatabase() {
    abstract fun customerDao(): io.decapos.posix.database.dao.CustomerDao
    abstract fun orderDao(): io.decapos.posix.database.dao.OrderDao
    abstract fun orderItemDao(): io.decapos.posix.database.dao.OrderItemDao
    abstract fun productDao(): io.decapos.posix.database.dao.ProductDao
    abstract fun promoDao(): io.decapos.posix.database.dao.PromoDao
    abstract fun userDao(): io.decapos.posix.database.dao.UserDao
    abstract fun printerDao(): io.decapos.posix.database.dao.PrinterDao
    abstract fun cartItemDao(): io.decapos.posix.database.dao.CartItemDao
}
