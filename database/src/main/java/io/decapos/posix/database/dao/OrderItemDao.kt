package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

import io.decapos.posix.vo.OrderItem
/** Used for querying OrderItem table from DB. */
@Dao
interface OrderItemDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderItem(orderItem: List<OrderItem>): List<Long>

    @Query("select * from orderItem where idOrderItem = :id")
    fun findOrderItemById(id: Int): OrderItem?

    @Query("select * from orderItem order by createdAt asc")
    fun getAllOrderItem(): LiveData<List<OrderItem>>

    @Query("delete from orderItem where idOrderItem = :id")
    fun deleteOrderItemById(id: Int): Int
}
