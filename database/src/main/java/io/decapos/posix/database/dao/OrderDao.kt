package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update

import io.decapos.posix.vo.Order
import io.decapos.posix.vo.OrderItem
import io.decapos.posix.vo.OrdersWithOrderItems
import io.decapos.posix.vo.ProductTotalOrdered
import org.threeten.bp.Instant

/** Used for querying Order table from DB. */
@Dao
interface OrderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrder(order: Order): Long

    @Query("select * from 'order' order by datetime(createdAt) desc")
    fun getAllOrder(): LiveData<List<Order>>

    @Query("delete from 'order' where idOrder = :id")
    fun deleteOrderById(id: Int): Int

    @Query("select * from 'order' where createdAt >= :dateStart and createdAt < :dateEnd")
    fun getOrderByDate(dateStart: Instant, dateEnd: Instant): LiveData<List<Order>>

    @Query("select count(*) from `Order`")
    fun getOrderCount(): LiveData<Int>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrderItem(orderItem: List<OrderItem>): List<Long>

    @Transaction
    suspend fun insertOrderWithOrderItems(ordersWithOrderItems: OrdersWithOrderItems): Int {
        val idOrder = insertOrder(ordersWithOrderItems.order)
        ordersWithOrderItems.orderItem.forEach {
            it.idOrder = idOrder.toInt()
        }
        insertOrderItem(ordersWithOrderItems.orderItem)
        return idOrder.toInt()
    }

    @Transaction
    @Query("select * from 'order' where idOrder = :id")
    fun getOrderWithOrderItem(id: Int): OrdersWithOrderItems

    @Transaction
    @Query("select * from 'order' where idOrder = :id")
    fun getOrderWithOrderItemLiveData(id: Int): LiveData<OrdersWithOrderItems>

    @Transaction
    @Query("SELECT * from `Order`")
    suspend fun getAllOrderWithOrderItems(): List<OrdersWithOrderItems>

    @Transaction
    @Query("SELECT * from `Order`")
    fun getAllOrderWithOrderItemsLiveData(): LiveData<List<OrdersWithOrderItems>>

    @Update
    suspend fun updateOrder(newOrder: Order)

    @Query(
        """
        SELECT name as productName, SUM(quantity) as totalOrdered 
        FROM `OrderItem` 
        GROUP BY name
        ORDER BY totalOrdered
        """
    )
    fun getAllProductTotalOrdered() : LiveData<List<ProductTotalOrdered>>
}
