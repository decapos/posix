package io.decapos.posix.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import io.decapos.posix.vo.Product
import io.decapos.posix.vo.ProductsWithCartItem

/** Used for querying Product table from DB. */
@Dao
interface ProductDao {
    @Query("select * from product where idProduct = :id")
    fun findProductById(id: Int): LiveData<Product>

    @Query("select * from product order by name asc")
    fun getAllProduct(): LiveData<List<Product>>

    @Query("select * from product order by name asc")
    fun getAllProductCoroutine(): List<Product>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(product: Product): Long

    @Query("delete from product where idProduct = :id")
    suspend fun deleteProductById(id: Int): Int

    @Transaction
    @Query("SELECT * FROM Product WHERE name LIKE :name ORDER BY name ASC")
    fun loadProductsWithCartItemByName(name: String): LiveData<List<ProductsWithCartItem>>

    @Query(
        """
        update product
        set price = :price
        where idProduct = :id
        """
    )
    suspend fun updatePriceById(id: Int, price: Long): Int

    @Query(
        """
        update product
        set name = :name
        where idProduct = :id
        """
    )
    suspend fun updateNameById(id: Int, name: String): Int

    @Query(
        """
        update product
        set imagePath = :path
        where idProduct = :id
        """
    )
    suspend fun updateImageById(id: Int, path: String): Int
}
