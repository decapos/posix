package io.decapos.posix.database.dagger

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.room.Room
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {
    /**
     * Provides DB instance.
     */
    @Singleton
    @Provides
    fun provideDb(app: Application): io.decapos.posix.database.PosixDb = Room
        .databaseBuilder(app, io.decapos.posix.database.PosixDb::class.java, "posix.db")
        .addMigrations(
            io.decapos.posix.database.Migration_4_5,
            io.decapos.posix.database.Migration_5_6,
            io.decapos.posix.database.Migration_6_7
        )
        .build()

    @Singleton
    @Provides
    fun provideProductDao(db: io.decapos.posix.database.PosixDb): io.decapos.posix.database.dao.ProductDao =
        db.productDao()

    @Singleton
    @Provides
    fun provideOrderDao(db: io.decapos.posix.database.PosixDb): io.decapos.posix.database.dao.OrderDao = db.orderDao()

    @Singleton
    @Provides
    fun provideOrderItemDao(db: io.decapos.posix.database.PosixDb): io.decapos.posix.database.dao.OrderItemDao =
        db.orderItemDao()

    @Singleton
    @Provides
    fun provideCartItemDao(db: io.decapos.posix.database.PosixDb): io.decapos.posix.database.dao.CartItemDao =
        db.cartItemDao()

    @Singleton
    @Provides
    fun provideSharedPreference(app: Application): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(app)

    @Singleton
    @Provides
    fun providePrinterDao(db: io.decapos.posix.database.PosixDb): io.decapos.posix.database.dao.PrinterDao =
        db.printerDao()
}