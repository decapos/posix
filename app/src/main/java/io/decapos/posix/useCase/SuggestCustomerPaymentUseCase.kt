package io.decapos.posix.useCase

import javax.inject.Inject
import kotlin.math.ceil

class SuggestCustomerPaymentUseCase @Inject constructor() {
    // Rupiah specific denomination
    // TODO: Might need to be refactored to support multi currency
    private val denominations = arrayOf(
        100_000L,
        50_000L,
        20_000L,
        10_000L,
        5000L,
        2000L,
        1000L,
        500L,
        200L,
        100L
    )

    operator fun invoke(price: Long): List<Long> {
        val result = mutableListOf<Long>()

        // Find smallest denomination that can cover the price
        val closestDenomination = denominations.findLast { it >= price } ?: 0
        val possibleSingleBill = denominations.filter { it > price && it != closestDenomination }

        // Find amount that can cover the price only using the biggest bill
        val possiblePayments = calculateAboveBiggestDenomination(
            price,
            0,
            if (closestDenomination == 0L) 0 else denominations.indexOf(closestDenomination)
        )
        val sortedPossibility = possiblePayments.groupBy { it }
            .map { Pair(it.value.size, it.key) }
            .sortedBy { it.second }
            .sortedByDescending { it.first }
            .map { it.second }

        result.addAll(possibleSingleBill)
        result.addAll(sortedPossibility)
        return result
    }

    private fun calculateAboveBiggestDenomination(
        price: Long,
        base: Long = 0,
        currentDenominationIndex: Int = 0
    ): Array<Long> {
        val differenceToCover = price - base
        val currentDenomination = denominations[currentDenominationIndex]
        val possiblePayment = if (differenceToCover <= currentDenomination) {
            currentDenomination + base
        } else {
            ceil(differenceToCover / currentDenomination.toDouble()).toLong() * currentDenomination + base
        }
        if (currentDenominationIndex == denominations.size - 1) {
            return if (possiblePayment == price) arrayOf() else arrayOf(possiblePayment)
        }
        val nextBase = possiblePayment - currentDenomination
        if (possiblePayment == price) return arrayOf(
            *calculateAboveBiggestDenomination(
                price,
                nextBase,
                currentDenominationIndex + 1
            )
        )
        return arrayOf(
            possiblePayment,
            *calculateAboveBiggestDenomination(
                price,
                nextBase,
                currentDenominationIndex + 1
            )
        )
    }
}
