package io.decapos.posix.useCase

import com.opencsv.CSVWriter
import io.decapos.posix.repository.OrderRepository
import java.io.Writer
import javax.inject.Inject

/**
 * Handles exporting of orders and its order items to a csv file
 * */
class ExportOrdersUseCase @Inject constructor(
    private val orderRepository: OrderRepository
) {
    suspend operator fun invoke(writer: Writer) {
        val orders = orderRepository.getAllOrderWithOrderItems()

        // get all product names
        val products = orders.flatMap { it.orderItem }
            .map { it.name }
            .distinct()
            .toTypedArray()

        val header = arrayOf(
            "id",
            *products,
            "total price",
            "total payment",
            "date"
        )

        // Create row of data
        val rows = orders.map {
            val row = Array(header.size) { "0" }
            row.apply {
                set(0, it.order.idOrder.toString())
                it.orderItem.forEach {
                    val index = header.indexOf(it.name)
                    set(index, it.quantity.toString())
                }
                set(header.size - 3, it.order.totalPayAmount.toString())
                set(header.size - 2, it.order.actualPayAmount.toString())
                set(header.size - 1, it.order.createdAt.toString())
            }
        }

        val csvWriter = CSVWriter(writer)
        csvWriter.use {
            it.writeNext(header)
            it.writeAll(rows)
        }
    }
}