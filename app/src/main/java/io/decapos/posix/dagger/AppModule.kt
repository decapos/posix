package io.decapos.posix.dagger

import android.app.Application
import com.mixpanel.android.mpmetrics.MixpanelAPI
import dagger.Module
import dagger.Provides

/**
 *  This module provides the singleton dependencies that the app requires (dao, db, etc).
 */
@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    fun provideMixpanel(app: Application): MixpanelAPI =
        MixpanelAPI.getInstance(app, "c8ef08b4b1295bc908d7cefa907768c2")
}
