package io.decapos.posix.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.decapos.posix.ui.bluetoothDevices.BluetoothDevicesViewModel
import io.decapos.posix.ui.cashierCheckout.CashierCheckoutViewModel
import io.decapos.posix.ui.cashierContainer.CashierContainerViewModel
import io.decapos.posix.ui.cashierSelectProduct.CashierSelectProductViewModel
import io.decapos.posix.ui.community.CommunityViewModel
import io.decapos.posix.ui.editOrder.EditOrderViewModel
import io.decapos.posix.ui.editProduct.EditProductViewModel
import io.decapos.posix.ui.managePrinters.ManagePrintersViewModel
import io.decapos.posix.ui.manageProducts.ManageProductsViewModel
import io.decapos.posix.ui.managePromos.ManagePromosViewModel
import io.decapos.posix.ui.monthlyBreakdown.MonthlyBreakdownViewModel
import io.decapos.posix.ui.monthlyReport.MonthlyReportViewModel
import io.decapos.posix.ui.newProduct.NewProductViewModel
import io.decapos.posix.ui.orderHistory.OrderHistoryViewModel
import io.decapos.posix.ui.ordersContainer.OrdersContainerViewModel
import io.decapos.posix.ui.promoContainer.PromoContainerViewModel
import io.decapos.posix.ui.reportContainer.ReportContainerViewModel
import io.decapos.posix.ui.reportDashboard.ReportDashboardViewModel
import io.decapos.posix.utils.PosixViewModelFactory

/**
 * Provides all of the ViewModel.
 *
 * As more viewModels are created it will be added here with @IntoMap annotation.
 * so that it will be provided to PosixViewModelFactory and be used in the fragments and activities.
 */
@Suppress("unused", "TooManyFunctions")
@Module
interface ViewModelModule {
    // IntoMap is a Dagger 2 multi-binding feature, for adding items into a Map.
    // See: https://google.github.io/dagger/multibindings
    /**
     * Provides SignInViewModel for usage in PosixViewModelFactory.
     */
    @Binds
    @IntoMap
    @ViewModelKey(CashierContainerViewModel::class)
    fun bindCashierContainerViewModel(viewModel: CashierContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PromoContainerViewModel::class)
    fun bindPromoContainerViewModel(viewModel: PromoContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReportContainerViewModel::class)
    fun bindReportContainerViewModel(viewModel: ReportContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageProductsViewModel::class)
    fun bindManageProductsViewModel(viewModel: ManageProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManagePromosViewModel::class)
    fun bindManagePromosViewModel(viewModel: ManagePromosViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewProductViewModel::class)
    fun bindNewProductViewModel(viewModel: NewProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditProductViewModel::class)
    fun bindEditProductViewModel(viewModel: EditProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReportDashboardViewModel::class)
    fun bindReportDashboardViewModel(viewModel: ReportDashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderHistoryViewModel::class)
    fun orderHistoryViewModel(viewModel: OrderHistoryViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MonthlyReportViewModel::class)
    fun bindMonthlyReportViewModel(viewModel: MonthlyReportViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManagePrintersViewModel::class)
    fun bindManagePrintersViewModel(viewModel: ManagePrintersViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CommunityViewModel::class)
    fun bindCommunityViewModel(viewModel: CommunityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BluetoothDevicesViewModel::class)
    fun bindBluetoothDevicesViewModel(viewModel: BluetoothDevicesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CashierSelectProductViewModel::class)
    fun bindCashierSelectProductViewModel(viewModel: CashierSelectProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CashierCheckoutViewModel::class)
    fun bindCashierCheckoutViewModel(viewModel: CashierCheckoutViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrdersContainerViewModel::class)
    fun bindOrdersContainerViewModel(viewModel: OrdersContainerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditOrderViewModel::class)
    fun bindEditOrdersViewModel(viewModel: EditOrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MonthlyBreakdownViewModel::class)
    fun bindMonthlyBreakdownViewModel(viewModel: MonthlyBreakdownViewModel): ViewModel

    /**
     * Provides ViewModel Factory.
     */
    @Binds
    fun bindViewModelFactory(factory: PosixViewModelFactory): ViewModelProvider.Factory
}
