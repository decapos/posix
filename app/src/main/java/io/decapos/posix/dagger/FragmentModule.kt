package io.decapos.posix.dagger

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.decapos.posix.ui.AdminContainerFragment
import io.decapos.posix.ui.AdminMenuFragment
import io.decapos.posix.ui.CustomerContainerFragment
import io.decapos.posix.ui.MainContainerFragment
import io.decapos.posix.ui.SettingsFragment
import io.decapos.posix.ui.bluetoothDevices.BluetoothDevicesFragment
import io.decapos.posix.ui.cashierCheckout.CashierCheckoutFragment
import io.decapos.posix.ui.cashierContainer.CashierContainerFragment
import io.decapos.posix.ui.cashierSelectProduct.CashierSelectProductFragment
import io.decapos.posix.ui.community.CommunityFragment
import io.decapos.posix.ui.editOrder.EditOrderFragment
import io.decapos.posix.ui.editProduct.EditProductFragment
import io.decapos.posix.ui.managePrinters.ManagePrintersFragment
import io.decapos.posix.ui.manageProducts.ManageProductsFragment
import io.decapos.posix.ui.managePromos.ManagePromosFragment
import io.decapos.posix.ui.monthlyBreakdown.MonthlyBreakdownFragment
import io.decapos.posix.ui.monthlyReport.MonthlyReportFragment
import io.decapos.posix.ui.newProduct.NewProductFragment
import io.decapos.posix.ui.orderHistory.OrderHistoryFragment
import io.decapos.posix.ui.ordersContainer.OrdersContainerFragment
import io.decapos.posix.ui.promoContainer.PromoContainerFragment
import io.decapos.posix.ui.reportContainer.ReportContainerFragment
import io.decapos.posix.ui.reportDashboard.ReportDashboardFragment

/**
 * This Provides the injector for fragments
 * See: https://google.github.io/dagger/android
 */
@Suppress("unused", "TooManyFunctions")
@Module
interface FragmentModule {

    /**
     * Provides injector for SignIn fragment.
     */
    @Suppress("FunctionMaxLength")
    @ContributesAndroidInjector
    fun contributeMainContainerFragmentInjector(): MainContainerFragment

    @ContributesAndroidInjector
    fun contributeCashierContainerFragmentInjector(): CashierContainerFragment

    @ContributesAndroidInjector
    fun contributePromoContainerFragmentInjector(): PromoContainerFragment

    @ContributesAndroidInjector
    fun contributeCustomerContainerFragmentInjector(): CustomerContainerFragment

    @ContributesAndroidInjector
    fun contributeReportContainerFragmentInjector(): ReportContainerFragment

    @ContributesAndroidInjector
    fun contributeAdminContainerFragmentInjector(): AdminContainerFragment

    @ContributesAndroidInjector
    fun contributeAdminMenuFragmentInjector(): AdminMenuFragment

    @ContributesAndroidInjector
    fun contributeManageProductsFragmentInjector(): ManageProductsFragment

    @ContributesAndroidInjector
    fun contributeManagePromosFragmentInjector(): ManagePromosFragment

    @ContributesAndroidInjector
    fun contributeNewProductFragmentInjector(): NewProductFragment

    @ContributesAndroidInjector
    fun contributeEditProductFragmentInjector(): EditProductFragment

    @ContributesAndroidInjector
    fun contributeReportDashboardFragmentInjector(): ReportDashboardFragment

    @ContributesAndroidInjector
    fun contributeOrderHistoryFragmentInjector(): OrderHistoryFragment

    @ContributesAndroidInjector
    fun contributeMonthlyReportFragmentInjector(): MonthlyReportFragment

    @ContributesAndroidInjector
    fun contributeManagePrintersFragmentInjector(): ManagePrintersFragment

    @ContributesAndroidInjector
    fun contributeCommunityFragmentInjector(): CommunityFragment

    @ContributesAndroidInjector
    fun contributeBluetoothDevicesFragmentInjector(): BluetoothDevicesFragment

    @ContributesAndroidInjector
    fun contributeSettingsFragmentInjector(): SettingsFragment

    @ContributesAndroidInjector
    fun contributeCashierSelectProductFragmentInjector(): CashierSelectProductFragment

    @ContributesAndroidInjector
    fun contributeCashierCheckoutFragmentInjector(): CashierCheckoutFragment

    @ContributesAndroidInjector
    fun contributeOrdersContainerFragmentInjector(): OrdersContainerFragment

    @ContributesAndroidInjector
    fun contributeEditOrdersFragmentInjector(): EditOrderFragment

    @ContributesAndroidInjector
    fun contributeMonthlyBreakdownFragmentInjector(): MonthlyBreakdownFragment
}
