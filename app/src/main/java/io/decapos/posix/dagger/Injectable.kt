package io.decapos.posix.dagger

/** Indicate that fragment is injectable by Dagger. */
interface Injectable
