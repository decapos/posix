package io.decapos.posix.dagger

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import io.decapos.posix.PosixApp
import io.decapos.posix.repository.dagger.RepositoryComponent
import io.decapos.posix.shared.dagger.AppScope

/** Entry point for dependency injection. Dagger will generate a component that will be able to inject
 *  dependencies into the android application class based on this, called DaggerAppComponent.
 *
 *  See: https://google.github.io/dagger/
 */
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        AppModule::class
    ],
    dependencies = [RepositoryComponent::class]
)
@AppScope
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun repositoryComponent(component: RepositoryComponent): Builder

        fun build(): AppComponent
    }

    fun inject(app: PosixApp)
}
