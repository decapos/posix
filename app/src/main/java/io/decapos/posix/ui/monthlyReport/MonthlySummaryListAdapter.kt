package io.decapos.posix.ui.monthlyReport

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.decapos.posix.databinding.ItemMonthlyReportSummaryBinding
import io.decapos.posix.ui.common.DataBoundListAdapter
import io.decapos.posix.vo.MonthlySummary

class MonthlySummaryListAdapter(
    appExecutors: io.decapos.posix.shared.utils.AppExecutors,
    private val callback: (MonthlySummary) -> Unit
) : DataBoundListAdapter<MonthlySummary, ItemMonthlyReportSummaryBinding>(
    appExecutors, MonthlySummaryDiffUtil()
) {
    override fun createBinding(parent: ViewGroup) = ItemMonthlyReportSummaryBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )

    override fun bind(binding: ItemMonthlyReportSummaryBinding, item: MonthlySummary) {
        binding.summary = item
        binding.containerItemMonthlyReport.setOnClickListener { callback.invoke(item) }
    }
}

private class MonthlySummaryDiffUtil : DiffUtil.ItemCallback<MonthlySummary>() {
    override fun areItemsTheSame(oldItem: MonthlySummary, newItem: MonthlySummary) =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: MonthlySummary, newItem: MonthlySummary) =
        oldItem.revenue == newItem.revenue
}