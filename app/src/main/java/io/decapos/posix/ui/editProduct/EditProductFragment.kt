package io.decapos.posix.ui.editProduct

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.mixpanel.android.mpmetrics.MixpanelAPI
import com.squareup.picasso.Picasso
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentEditProductBinding
import io.decapos.posix.shared.utils.attachCurrencyFormatter
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.hideKeyboard
import javax.inject.Inject

class EditProductFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mixpanel: MixpanelAPI

    private val viewModel by viewModels<EditProductViewModel> { viewModelFactory }

    private val args: EditProductFragmentArgs by navArgs()

    var binding by autoCleared<FragmentEditProductBinding>()

    private val PICK_IMAGE = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewModel.id.value = args.id
        binding = FragmentEditProductBinding.inflate(inflater, container, false)
        binding.textFieldUpdatePrice.attachCurrencyFormatter(viewLifecycleOwner)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.btnSaveChanges.setOnClickListener { saveProductChanges() }
        binding.btnDelete.setOnClickListener { deleteProduct() }

        //TODO: image still not loaded because product is null
        Picasso.get()
            .load(viewModel.product.value?.imagePath)
            .placeholder(R.drawable.image_placeholder_product)
            .into(binding.thumbnailProduct)

        binding.thumbnailProduct.setOnClickListener { launchImageSelector() }

        return binding.root
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    private fun saveProductChanges() {
        viewModel.saveProductChanges().invokeOnCompletion {
            if (viewModel.errorMsg.value.isNullOrEmpty()) {
                mixpanel.track("Edit product")
                findNavController().navigate(R.id.edit_product_to_manage_products)
            }
        }
    }

    private fun deleteProduct() {
        val directions = EditProductFragmentDirections.editProductToManageProducts()
        directions.deletedProduct = viewModel.product.value
        viewModel.deleteProduct()
        findNavController().navigate(directions)
    }

    private fun launchImageSelector() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            setType("image/*")
        }
        startActivityForResult(Intent.createChooser(intent, "Select image"), PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            val imagePath = data?.data

            Picasso.get()
                .load(imagePath)
                .placeholder(R.drawable.image_placeholder_product)
                .into(binding.thumbnailProduct)

            viewModel.imageUri = imagePath?.toString()
        }
    }
}