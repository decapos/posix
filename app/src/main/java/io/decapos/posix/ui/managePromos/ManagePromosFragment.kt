package io.decapos.posix.ui.managePromos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable

class ManagePromosFragment : Fragment(), Injectable {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_manage_promos, container, false)
    }
}