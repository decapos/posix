package io.decapos.posix.ui.signIn

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class SignInViewModel @Inject constructor() : ViewModel()
