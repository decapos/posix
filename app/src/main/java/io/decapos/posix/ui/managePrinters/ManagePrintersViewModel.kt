package io.decapos.posix.ui.managePrinters

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import io.decapos.posix.repository.PreferenceRepository
import io.decapos.posix.repository.PrinterRepository
import io.decapos.posix.utils.BluetoothHelper
import io.decapos.posix.vo.Printer
import javax.inject.Inject

class ManagePrintersViewModel @Inject constructor(
    private val printerRepo: PrinterRepository,
    preferenceRepo: PreferenceRepository,
    orderRepo: OrderRepository
) : ViewModel() {
    private val bluetoothHelper = BluetoothHelper(printerRepo, preferenceRepo, orderRepo)

    private val printers = printerRepo.getUserPrinters()

    val bluetoothDevices = printerRepo.getPairedPrinter()

    private val defaultPrinterAddress = preferenceRepo.getDefaultPrinterLiveData()

    val defaultPrinter = MediatorLiveData<List<Pair<Printer, Boolean>>>().apply {
        val callback = { printers: List<Printer>?, defaultPrinterAddress: String? ->
            if (printers != null && defaultPrinterAddress != null) value = printers.map { printer ->
                Pair(printer, printer.address == defaultPrinterAddress)
            }
        }
        addSource(printers) { callback(it, defaultPrinterAddress.value) }
        addSource(defaultPrinterAddress) { callback(printers.value, it) }
    }

    fun testPrint(address: String) {
        bluetoothHelper.testPrint(address)
    }

    fun deletePrinter(id: Int) {
        printerRepo.deletePrinterById(id)
    }

    fun setDefault(address: String) {
        bluetoothHelper.setPrinterDefault(address)
    }
}