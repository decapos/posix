package io.decapos.posix.ui.cashierCheckout

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import io.decapos.posix.repository.CartRepository
import io.decapos.posix.shared.utils.Converter
import io.decapos.posix.useCase.SuggestCustomerPaymentUseCase
import io.decapos.posix.utils.BluetoothHelper
import io.decapos.posix.vo.CartItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class CashierCheckoutViewModel @Inject constructor(
    private val repository: CartRepository,
    private val bluetoothHelper: BluetoothHelper,
    private val paymentSuggestionUseCase: SuggestCustomerPaymentUseCase
) : ViewModel() {

    val cartItems = repository.loadCartItemsWithProducts()

    val discount = MutableLiveData("0")

    val totalPrice = cartItems.map {
        it.fold(0L) { acc, item ->
            acc + item.cartDetail.quantity * item.product.first().price
        }
    }

    val totalPriceWithDiscount = MediatorLiveData<Long>().apply {
        val calculateDiscounted = { totalPrice: Long, discount: String? ->
            val discountInLong = if (discount.isNullOrBlank()) 0 else discount.toInt()
            totalPrice - totalPrice * discountInLong / 100
        }
        addSource(discount) { value = calculateDiscounted(totalPrice.value ?: 0, it) }
        addSource(totalPrice) { value = calculateDiscounted(it, discount.value) }
    }

    val totalDiscountAmount = totalPriceWithDiscount.map { totalPrice.value?.minus(it) ?: 0 }

    val userPaymentSuggestion = totalPrice.map { paymentSuggestionUseCase(it) }

    val selectedUserPaymentSuggestion = MutableLiveData<String>()

    val userPayment = MediatorLiveData<String>().apply {
        addSource(totalPrice) { value = Converter.longToStringCurrency(it) }
        addSource(selectedUserPaymentSuggestion) {
            value = if (it.isBlank()) {
                Converter.longToStringCurrency(totalPrice.value ?: 0)
            } else {
                it
            }
        }
    }

    private val userPaymentLong = userPayment.map { Converter.stringCurrencyToLong(it) }

    val isEnabled = userPayment.map {
        userPaymentLong.value ?: 0 >= totalPrice.value ?: 0
    }

    val change = MediatorLiveData<Long>().apply {
        addSource(userPaymentLong) {
            value = it - (totalPrice.value ?: 0)
        }
    }

    fun updateProduct(item: CartItem) {
        repository.updateCartItem(item)
    }

    fun pay() = CoroutineScope(IO).launch {
        cartItems.value?.let {
            val amount = userPaymentLong.value ?: 0
            val orderId = repository.processOrder(
                it,
                totalPriceWithDiscount.value ?: 0L,
                userPaymentLong.value ?: 0L
            )
            bluetoothHelper.printBill(orderId, amount)
        }
    }

    fun setPrinterDefault(address: String) {
        bluetoothHelper.setPrinterDefault(address)
    }
}