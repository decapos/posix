package io.decapos.posix.ui.cashierContainer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import io.decapos.posix.repository.ProductRepository
import javax.inject.Inject

class CashierContainerViewModel @Inject constructor(
    productRepository: ProductRepository
) : ViewModel() {
    val isProductNotEmpty = productRepository.getAllProduct().map {
        it.isNotEmpty()
    }
}
