package io.decapos.posix.ui.promoContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable

class PromoContainerFragment : Fragment(), Injectable {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_container_promo, container, false)
    }
}
