package io.decapos.posix.ui.cashierContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import kotlinx.android.synthetic.main.fragment_container_cashier.view.*
import javax.inject.Inject

class CashierContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<CashierContainerViewModel> { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = layoutInflater.inflate(R.layout.fragment_container_cashier, container, false)
        viewModel.isProductNotEmpty.observe(this) {
            view.empty_switcher.displayedChild = if (it) 1 else 0
            view.empty_switcher.visibility = View.VISIBLE
        }

        // Navigate directly to ManageProductFragment inside admin section
        view.btn_manage_product.setOnClickListener {
            val action = CashierContainerFragmentDirections
                .cashierContainerToAdminContainer()
            findNavController().navigate(action)
        }

        return view
    }
}
