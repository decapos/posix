package io.decapos.posix.ui.editOrder

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import io.decapos.posix.shared.utils.Converter
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class EditOrderViewModel @Inject constructor(
    private val orderRepo: OrderRepository
) : ViewModel() {
    fun deleteOrder(idOrder: Int) = CoroutineScope(IO).launch {
        orderRepo.deleteOrder(idOrder)
    }

    fun saveOrder() = CoroutineScope(IO).launch {
        val newOrder = orderWithOrderItems.value?.order?.copy(
            actualPayAmount = Converter.stringCurrencyToLong(actualPayAmount.value),
            totalPayAmount = Converter.stringCurrencyToLong(totalPrice.value)
        )
        orderRepo.updateOrder(newOrder!!)
    }

    val idOrder = MutableLiveData<Int>()

    val orderWithOrderItems = Transformations.switchMap(idOrder) {
        orderRepo.getOrderWithOrderItemsLiveData(it)
    }

    val actualPayAmount = MediatorLiveData<String>().apply {
        addSource(orderWithOrderItems) {
            value = Converter.longToStringCurrency(it.order.actualPayAmount)
        }
    }

    val totalPrice = MediatorLiveData<String>().apply {
        addSource(orderWithOrderItems) {
            value = Converter.longToStringCurrency(it.order.totalPayAmount)
        }
    }
}