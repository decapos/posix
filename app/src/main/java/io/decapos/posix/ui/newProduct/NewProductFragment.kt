package io.decapos.posix.ui.newProduct

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.mixpanel.android.mpmetrics.MixpanelAPI
import com.squareup.picasso.Picasso
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentNewProductBinding
import io.decapos.posix.shared.utils.attachCurrencyFormatter
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.hideKeyboard
import javax.inject.Inject

class NewProductFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mixpanel: MixpanelAPI

    private val viewModel by viewModels<NewProductViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentNewProductBinding>()

    private val PICK_IMAGE = 1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentNewProductBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewmodel = viewModel
        binding.textFieldNewPrice.attachCurrencyFormatter(viewLifecycleOwner)
        binding.btnAddProduct.setOnClickListener { saveNewProduct() }
        binding.thumbnailProduct.setOnClickListener { launchImageSelector() }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        hideKeyboard()
    }

    private fun saveNewProduct() {
        mixpanel.track("Add new product")
        viewModel.saveNewProduct().invokeOnCompletion {
            if (viewModel.errorMsg.value.isNullOrEmpty()) {
                findNavController().navigate(R.id.new_product_to_manage_product)
            }
        }
    }

    private fun launchImageSelector() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            setType("image/*")
        }
        startActivityForResult(Intent.createChooser(intent, "Select image"), PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            val imagePath = data?.data

            Picasso.get()
                .load(imagePath)
                .placeholder(R.drawable.image_placeholder_product)
                .into(binding.thumbnailProduct)

            viewModel.imageUri = imagePath?.toString()
        }
    }
}
