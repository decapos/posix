package io.decapos.posix.ui.newProduct

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.decapos.posix.repository.ProductRepository
import io.decapos.posix.shared.utils.Converter
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class NewProductViewModel @Inject constructor(
    private val productRepository: ProductRepository
) : ViewModel() {
    val name = MutableLiveData<String>()

    val price = MutableLiveData<String>()

    val errorMsg = MutableLiveData<String>()

    var imageUri: String? = ""

    fun saveNewProduct() = viewModelScope.launch {
        val name = name.value ?: ""
        val price = price.value ?: ""
        if (name == "" || price == "") {
            // TODO: Move this to strings.xml
            errorMsg.value = "Semua field harus diisi"
        } else {
            val finalPrice = Converter.stringCurrencyToLong(price)
            withContext(IO) {
                productRepository.insert(name, finalPrice, imageUri ?: "")
            }
        }
        errorMsg.value = null
    }
}

