package io.decapos.posix.ui.managePrinters

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentManagePrintersBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.ui.managePrinters.ListPrinterAdapter.Companion.DELETE_PRINTER
import io.decapos.posix.ui.managePrinters.ListPrinterAdapter.Companion.SET_DEFAULT
import io.decapos.posix.ui.managePrinters.ListPrinterAdapter.Companion.TEST_PRINT
import javax.inject.Inject

class ManagePrintersFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var mixpanel: MixpanelAPI

    @Inject
    lateinit var appExecutors: AppExecutors

    private var mBluetoothAdapter: BluetoothAdapter? = null

    private val viewModel by viewModels<ManagePrintersViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentManagePrintersBinding>()

    private var adapter by autoCleared<ListPrinterAdapter>()

    private val REQUEST_ENABLE_BT = 3

    private var isBluetoothFabShown = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentManagePrintersBinding.inflate(inflater, container, false)

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        adapter = ListPrinterAdapter(appExecutors) { printer, type ->
            when (type) {
                TEST_PRINT -> viewModel.testPrint(printer.address)
                SET_DEFAULT -> viewModel.setDefault(printer.address)
                DELETE_PRINTER -> viewModel.deletePrinter(printer.idPrinter!!)
            }
        }
        binding.recyclerAllPrinters.adapter = adapter

        viewModel.defaultPrinter.observe(this) {
            adapter.submitList(it)
            binding.emptySwitcher.displayedChild = if (it.isNotEmpty()) 1 else 0
        }

        isBluetoothFabShown = false
        binding.fabAddPrinter.setOnClickListener {
            if (isBluetoothFabShown) hideBluetoothFab() else showBluetoothFab()
        }

        binding.fabBluetoothPrinter.setOnClickListener {
            mixpanel.track("Add printer")
            try {
                if (!mBluetoothAdapter!!.isEnabled) {
                    val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT)
                } else {
                    findNavController().navigate(R.id.manage_printers_to_bluetooth_devices)
                }
            } catch (error: KotlinNullPointerException) {
                hideBluetoothFab()
                Snackbar.make(binding.root, R.string.bayar, LENGTH_LONG).show()
            }
        }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_ENABLE_BT && mBluetoothAdapter!!.isEnabled) {
            findNavController().navigate(R.id.manage_printers_to_bluetooth_devices)
        }
    }

    private fun showBluetoothFab() {
        binding.layoutBluetoothPrinter.visibility = View.VISIBLE
        binding.layoutCover.visibility = View.VISIBLE
        AnimatorSet().apply {
            play(ObjectAnimator.ofFloat(binding.fabAddPrinter, "rotation", 45f))
            play(
                ObjectAnimator.ofFloat(
                    binding.layoutBluetoothPrinter,
                    "translationY",
                    -40f
                )
            )
            duration = 200
            start()
        }
        isBluetoothFabShown = true
    }

    private fun hideBluetoothFab() {
        binding.layoutBluetoothPrinter.visibility = View.INVISIBLE
        binding.layoutCover.visibility = View.GONE
        AnimatorSet().apply {
            play(ObjectAnimator.ofFloat(binding.fabAddPrinter, "rotation", 0f))
            play(ObjectAnimator.ofFloat(binding.layoutBluetoothPrinter, "translationY", 0f))
            duration = 200
            start()
        }
        isBluetoothFabShown = false
    }
}