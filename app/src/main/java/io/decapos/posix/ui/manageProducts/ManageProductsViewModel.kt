package io.decapos.posix.ui.manageProducts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.decapos.posix.repository.ProductRepository
import io.decapos.posix.vo.Product
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class ManageProductsViewModel @Inject constructor(private val productRepository: ProductRepository) : ViewModel() {
    fun restoreProduct(product: Product) = viewModelScope.launch(IO) {
        productRepository.insert(product)
    }

    val products = productRepository.getAllProduct()

    var isDeleteUndoShown = false
}
