package io.decapos.posix.ui.editOrder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.decapos.posix.databinding.ItemOrderItemBinding
import io.decapos.posix.ui.common.DataBoundListAdapter
import io.decapos.posix.vo.OrderItem

class OrderItemListAdapter(
    executors: io.decapos.posix.shared.utils.AppExecutors,
    callback: (OrderItem) -> Unit
) : DataBoundListAdapter<OrderItem, ItemOrderItemBinding>(
    executors, OrderItemDiffCallback()
) {
    override fun createBinding(parent: ViewGroup): ItemOrderItemBinding {
        return ItemOrderItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    }

    override fun bind(binding: ItemOrderItemBinding, item: OrderItem) {
        binding.item = item
    }
}

private class OrderItemDiffCallback : DiffUtil.ItemCallback<OrderItem>() {
    override fun areItemsTheSame(oldItem: OrderItem, newItem: OrderItem) =
        oldItem.idOrderItem == newItem.idOrderItem

    override fun areContentsTheSame(oldItem: OrderItem, newItem: OrderItem) =
        oldItem == newItem
}