package io.decapos.posix.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import kotlinx.android.synthetic.main.fragment_container_admin.view.*

/**
 *  Top level fragment for admin section. Setups the navigation graph and app bar.
 * */
class AdminContainerFragment : Fragment(), Injectable {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_container_admin, container, false)

        val navHost = view.findViewById<View>(R.id.section_nav_host_fragment)
        val navController = Navigation.findNavController(navHost)
        view.toolbar.setupWithNavController(navController)

        // Syncs toolbar text with current fragment (since we use our own textview instead of the toolbar's title
        // so we can center the text).
        navController.addOnDestinationChangedListener { _, destination, _ ->
            view.toolbar_title.text = destination.label
        }
        return view
    }
}