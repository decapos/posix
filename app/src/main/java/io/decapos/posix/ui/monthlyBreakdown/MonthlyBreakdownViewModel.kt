package io.decapos.posix.ui.monthlyBreakdown

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import io.decapos.posix.vo.OrdersWithOrderItems
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId
import javax.inject.Inject

class MonthlyBreakdownViewModel @Inject constructor(
    orderRepo: OrderRepository
) : ViewModel() {
    private val orders = orderRepo.getAllOrderWithOrderItemsLiveData()

    val chosenMonth = MutableLiveData<YearMonth>()

    val ordersOnChosenMonth = MediatorLiveData<List<DailySummary>>().apply {
        val calculate = { yearMonth: YearMonth, orders: List<OrdersWithOrderItems> ->
            orders.filter {
                // select only orders from the chosen month
                YearMonth.from(it.order.createdAt.atZone(ZoneId.systemDefault())) == yearMonth
            }.groupBy { (order, _) ->
                // Group orders by day of month
                order.createdAt.atZone(ZoneId.systemDefault()).dayOfMonth
            }.map { (dayOfMonth, orders) ->
                // Create summarised data of the day
                DailySummary(
                    dayOfMonth,
                    yearMonth.month,
                    // Sum revenues from all orders
                    orders.foldRight(0L) { (order, _), acc -> order.totalPayAmount + acc }
                )
            }
        }
        addSource(chosenMonth) { month ->
            orders.value?.also {
                value = calculate(month, it)
            }
        }
        addSource(orders) { ordersWithOrderItems ->
            chosenMonth.value?.also {
                value = calculate(it, ordersWithOrderItems)
            }
        }
    }
}
