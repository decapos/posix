package io.decapos.posix.ui.bluetoothDevices

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.decapos.posix.repository.PrinterRepository
import io.decapos.posix.shared.utils.BluetoothBondStateLiveData
import io.decapos.posix.shared.utils.BluetoothLiveData
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class BluetoothDevicesViewModel @Inject constructor(
    private val printerRepo: PrinterRepository,
    bluetoothBondStateLiveData: BluetoothBondStateLiveData,
    bluetoothLiveData: BluetoothLiveData
): ViewModel() {
    var deviceName = ""
    var deviceAddress = ""

    val bluetoothDevices = bluetoothLiveData

    val bluetoothBondstate = bluetoothBondStateLiveData

    fun checkPrinter(it: BluetoothDevice) = viewModelScope.launch(IO) {
        if (printerRepo.findPrinterByAddress(it.address) === null) {
            if (it.bondState == BluetoothDevice.BOND_BONDED) addPrinter(it.name, it.address)
            else it.createBond()
        }
    }

    fun addPrinter(name: String, address: String) =
        printerRepo.insertToDb(name ?: "N/A", address)
}