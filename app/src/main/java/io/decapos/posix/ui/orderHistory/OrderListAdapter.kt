package io.decapos.posix.ui.orderHistory

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.decapos.posix.databinding.ItemOrderHistoryBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.ui.common.DataBoundListAdapter
import io.decapos.posix.vo.Order

class OrderListAdapter(
    appExecutors: AppExecutors,
    private val callback: ((order: Order) -> Unit)?
) : DataBoundListAdapter<Order, ItemOrderHistoryBinding>(
    appExecutors, OrderDiffUtil()
) {
    override fun createBinding(parent: ViewGroup) = ItemOrderHistoryBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
    )

    override fun bind(binding: ItemOrderHistoryBinding, item: Order) {
        binding.order = item
        binding.orderHistoryItem.setOnClickListener {
            callback?.invoke(item)
        }
    }
}

private class OrderDiffUtil : DiffUtil.ItemCallback<Order>() {
    override fun areItemsTheSame(oldItem: Order, newItem: Order) =
        oldItem.idOrder == newItem.idOrder

    override fun areContentsTheSame(oldItem: Order, newItem: Order) =
        oldItem.updatedAt == newItem.updatedAt
}
