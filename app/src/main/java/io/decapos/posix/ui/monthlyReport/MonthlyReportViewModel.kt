package io.decapos.posix.ui.monthlyReport

import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import javax.inject.Inject

class MonthlyReportViewModel @Inject constructor(
    orderRepository: OrderRepository
) : ViewModel() {
    val ordersByMonth = orderRepository.getMonthlySummary()
}
