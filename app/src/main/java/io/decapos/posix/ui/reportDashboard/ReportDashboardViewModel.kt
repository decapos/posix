package io.decapos.posix.ui.reportDashboard

import android.app.Application
import android.graphics.Color
import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.decapos.posix.repository.OrderRepository
import io.decapos.posix.shared.utils.Converter
import io.decapos.posix.useCase.ExportOrdersUseCase
import io.decapos.posix.utils.NotificationHelper
import io.decapos.posix.vo.ProductTotalOrdered
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileWriter
import javax.inject.Inject

class ReportDashboardViewModel @Inject constructor(
    orderRepo: OrderRepository,
    private val mixpanel: MixpanelAPI,
    private val exportOrders: ExportOrdersUseCase,
    private val app: Application,
    private val helper: NotificationHelper
) : ViewModel() {

    // Turns monthlySummary data into
    val monthlySummaryChartData = orderRepo.getMonthlySummary().map {
        it.take(6)
            .map { BarEntry(it.yearMonth.monthValue.toFloat(), it.revenue.toFloat()) }
            .run { BarDataSet(this, "") }
            .run {
                valueTextColor = Color.WHITE
                valueTextSize = 14f
                setColor(Color.WHITE, 143)
                BarData(this)
            }.apply {
                setValueFormatter(object : ValueFormatter() {
                    override fun getFormattedValue(value: Float) =
                        Converter.longToStringCurrency(value.toLong())
                })
            }
    }

    val productsTotalOrder = orderRepo.getAllProductTotalOrdered().map { item ->
        val totalItemSold = item.sumBy { it.totalOrdered.toInt() }
        // only show items that constitutes more than 10% of total items sold
        val significantItems = item.filter {
            it.totalOrdered > (10 / 100) * totalItemSold
        }.toTypedArray()
        val insignificantItemsSold =
            totalItemSold - significantItems.sumBy { it.totalOrdered.toInt() }

        // return data with less sold items compacted into "others"
        if (insignificantItemsSold == 0) {
            significantItems
        } else {
            arrayOf(
                *significantItems,
                ProductTotalOrdered("Other", insignificantItemsSold.toFloat())
            )
        }.map {
            PieEntry(it.totalOrdered, it.productName)
        }.run {
            PieDataSet(this, "Products")
        }.run {
            valueTextSize = 14f
            yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
            colors = ColorTemplate.PASTEL_COLORS.toMutableList()
            PieData(this)
        }.apply {
            setValueFormatter(object : ValueFormatter() {
                override fun getFormattedValue(value: Float) =
                    value.toInt().toString() + "%"
            })
        }
    }

    fun exportCsvHandler(view: View) {
        CoroutineScope(IO).launch {
            mixpanel.track("Export data")
            val file = File(app.getExternalFilesDir(null), "report.csv")
            val fileWriter = FileWriter(file)
            exportOrders(fileWriter)
            helper.notifyExport(file.absolutePath)
        }
    }
}
