package io.decapos.posix.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import kotlinx.android.synthetic.main.fragment_admin_menu.view.*

/**
 * Top level fragment for admin fragment
 * */
class AdminMenuFragment : Fragment(), Injectable {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_admin_menu, container, false)
        view.btn_edit_product.setOnClickListener {
            findNavController().navigate(R.id.admin_menu_to_manage_products)
        }
        view.btn_printer_setting.setOnClickListener {
            findNavController().navigate(R.id.admin_menu_to_manage_printers)
        }
        view.btn_settings.setOnClickListener {
            findNavController().navigate(R.id.admin_menu_to_settings)
        }
        SettingsFragment()
        return view
    }
}