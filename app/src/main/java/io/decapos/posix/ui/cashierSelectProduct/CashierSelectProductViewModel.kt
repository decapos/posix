package io.decapos.posix.ui.cashierSelectProduct

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import io.decapos.posix.repository.CartRepository
import io.decapos.posix.repository.ProductRepository
import io.decapos.posix.vo.CartItem
import io.decapos.posix.vo.Product
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class CashierSelectProductViewModel @Inject constructor(
    productRepo: ProductRepository,
    private val cartRepo: CartRepository
) : ViewModel() {

    val cartItems = cartRepo.loadCartItemsWithProducts()

    val searchQuery = MutableLiveData("")

    val isQueryNotEmpty = searchQuery.map { it.isNotEmpty() }

    val products = searchQuery.switchMap {
        productRepo.loadProductsWithCartItemsByName(it)
    }

    val totalPrice = cartItems.map {
        it.fold(0L) { acc, item ->
            acc + item.cartDetail.quantity * (item.product.firstOrNull()?.price ?: 0)
        }
    }

    val itemCount = cartItems.map { items ->
        items.fold(0) { acc, item -> acc + item.cartDetail.quantity }
    }

    fun addProductToCart(product: Product, cartDetails: CartItem?) {
        if (cartDetails == null) {
            cartRepo.insertProductToCart(product)
        } else {
            cartRepo.updateCartItem(cartDetails.run {
                copy(quantity = quantity + 1)
            })
        }
    }

    fun handleClearCart() = viewModelScope.launch(IO) {
        cartRepo.clearCartItems()
    }

    fun restoreCart() = cartRepo.restoreCartItems()
}
