package io.decapos.posix.ui.cashierCheckout

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.squareup.picasso.Picasso
import io.decapos.posix.R
import io.decapos.posix.databinding.ItemCheckoutProductBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.ui.common.DataBoundListAdapter
import io.decapos.posix.vo.CartItem
import io.decapos.posix.vo.CartItemsWithProduct

class CheckoutProductListAdapter(
    executors: AppExecutors,
    val callback: ((CartItem) -> Unit)?
) : DataBoundListAdapter<CartItemsWithProduct, ItemCheckoutProductBinding>(
    executors, CheckoutProductDiffCallback()
) {
    override fun createBinding(parent: ViewGroup): ItemCheckoutProductBinding {
        val binding = ItemCheckoutProductBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        binding.btnAddItem.setOnClickListener {
            binding.cartDetail?.apply {
                val newItem = copy(quantity = quantity + 1)
                callback?.invoke(newItem)
            }
        }
        binding.btnRemoveItem.setOnClickListener {
            binding.cartDetail?.apply {
                val newItem = copy(quantity = quantity - 1)
                callback?.invoke(newItem)
            }
        }
        return binding
    }

    override fun bind(binding: ItemCheckoutProductBinding, item: CartItemsWithProduct) {
        binding.product = item.product.first()
        //TODO: having delayed when load image using this method, find other way
        Picasso.get()
            .load(Uri.parse(item.product.first().imagePath))
            .placeholder(R.drawable.image_placeholder_product)
            .into(binding.imageView)
        binding.cartDetail = item.cartDetail
    }
}

private class CheckoutProductDiffCallback : DiffUtil.ItemCallback<CartItemsWithProduct>() {
    override fun areContentsTheSame(old: CartItemsWithProduct, new: CartItemsWithProduct) =
        old.cartDetail == new.cartDetail &&
            old.product.first().name == new.product.first().name &&
            old.product.first().price == new.product.first().price &&
            old.product.first().imagePath == new.product.first().imagePath

    override fun areItemsTheSame(old: CartItemsWithProduct, new: CartItemsWithProduct) =
        old.cartDetail.idCartItem == new.cartDetail.idCartItem
}
