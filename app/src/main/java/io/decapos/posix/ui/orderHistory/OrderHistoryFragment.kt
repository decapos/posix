package io.decapos.posix.ui.orderHistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.snackbar.Snackbar
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentOrderHistoryBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.observe
import javax.inject.Inject

class OrderHistoryFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private val viewModel by viewModels<OrderHistoryViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentOrderHistoryBinding>()

    private var adapter by autoCleared<OrderListAdapter>()

    private val args: OrderHistoryFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        adapter = OrderListAdapter(appExecutors) { navigateToEditOrder(it.idOrder) }
        viewModel.orders.observe(this) { adapter.submitList(it) }

        binding = FragmentOrderHistoryBinding.inflate(inflater, container, false)
        binding.recyclerAllTransaction.adapter = adapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (args.idOrder != -1) showUndoDelete(view, args.idOrder)
    }

    private fun navigateToEditOrder(idOrder: Int?) {
        if (idOrder == null) return
        val direction = OrderHistoryFragmentDirections.actionOrderHistoryFragmentToEditOrderFragment(idOrder)
        findNavController().navigate(direction)
    }

    private fun showUndoDelete(view: View, idOrder: Int) {
        if (viewModel.undoHasBeenShown) return
        Snackbar.make(view, getString(R.string.snackbar_order_deleted, idOrder), Snackbar.LENGTH_LONG)
            .setAction(R.string.undo) { viewModel.restoreLastDeletedOrder() }
            .show()
        viewModel.undoHasBeenShown = true
    }
}
