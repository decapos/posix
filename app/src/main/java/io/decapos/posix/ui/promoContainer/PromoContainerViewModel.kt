package io.decapos.posix.ui.promoContainer

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class PromoContainerViewModel @Inject constructor() : ViewModel()
