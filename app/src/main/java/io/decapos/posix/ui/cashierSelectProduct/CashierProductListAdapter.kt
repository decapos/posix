package io.decapos.posix.ui.cashierSelectProduct

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.squareup.picasso.Picasso
import io.decapos.posix.R
import io.decapos.posix.databinding.ItemProductBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.ui.common.DataBoundListAdapter
import io.decapos.posix.vo.CartItem
import io.decapos.posix.vo.Product
import io.decapos.posix.vo.ProductsWithCartItem

/**
 *  Adapter for showing list of products.
 * */
class CashierProductListAdapter(
    executors: AppExecutors,
    val callback: ((Product, CartItem?) -> Unit)?
) : DataBoundListAdapter<ProductsWithCartItem, ItemProductBinding>(
    executors, CashierProductDiffCallback()
) {
    override fun createBinding(parent: ViewGroup): ItemProductBinding {
        val binding = ItemProductBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        binding.containerProduct.setOnClickListener {
            binding.product?.let { product ->
                callback?.invoke(product, binding.cartDetail)
            }
        }
//        binding.image = placeholderBitmap
        return binding
    }

    override fun bind(binding: ItemProductBinding, item: ProductsWithCartItem) {
        binding.product = item.product
        //TODO: having delayed when load image using this method, find other way
        Picasso.get()
            .load(Uri.parse(item.product.imagePath))
            .placeholder(R.drawable.image_placeholder_product)
            .into(binding.thumbnailProduct)
        binding.cartDetail = item.cartDetail.firstOrNull()
    }
}

private class CashierProductDiffCallback : DiffUtil.ItemCallback<ProductsWithCartItem>() {
    override fun areContentsTheSame(old: ProductsWithCartItem, new: ProductsWithCartItem) =
        old.cartDetail == new.cartDetail &&
            old.product.name == new.product.name &&
            old.product.price == new.product.price &&
            old.product.imagePath == new.product.imagePath

    override fun areItemsTheSame(old: ProductsWithCartItem, new: ProductsWithCartItem) =
        old.product.idProduct == new.product.idProduct
}
