package io.decapos.posix.ui.monthlyBreakdown

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.ui.setupWithNavController
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentMonthlyBreakdownBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.observe
import org.threeten.bp.YearMonth
import javax.inject.Inject

class MonthlyBreakdownFragment : Fragment(), Injectable {

    @Inject
    lateinit var executor: AppExecutors

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<MonthlyBreakdownViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentMonthlyBreakdownBinding>()

    private var adapter by autoCleared<DailySummaryListAdapter>()

    private val args by navArgs<MonthlyBreakdownFragmentArgs>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        adapter = DailySummaryListAdapter(executor)
        viewModel.ordersOnChosenMonth.observe(this) { adapter.submitList(it) }
        viewModel.chosenMonth.value = YearMonth.parse(args.date)

        binding = FragmentMonthlyBreakdownBinding.inflate(inflater, container, false)
        binding.toolbar.setupWithNavController(findNavController())
        binding.listMonthlyOrder.adapter = adapter
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
}