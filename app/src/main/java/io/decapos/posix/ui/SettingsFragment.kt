package io.decapos.posix.ui

import android.os.Bundle
import androidx.preference.PreferenceFragmentCompat
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable

class SettingsFragment : PreferenceFragmentCompat(), Injectable {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
    }
}