package io.decapos.posix.ui.ordersContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.shared.utils.observe
import kotlinx.android.synthetic.main.fragment_container_orders.view.*
import javax.inject.Inject

class OrdersContainerFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<OrdersContainerViewModel> { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_container_orders, container, false)
        val navHost = view.findViewById<View>(R.id.section_nav_host_fragment)
        val navController = Navigation.findNavController(navHost)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            view.toolbar_title.text = destination.label
        }
        view.toolbar.setupWithNavController(navController)

        viewModel.isOrderEmpty.observe(this) {
            view.empty_view_switcher.displayedChild = if (it) 1 else 0
            view.empty_view_switcher.visibility = View.VISIBLE
        }
        return view
    }
}