package io.decapos.posix.ui.monthlyBreakdown

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.decapos.posix.databinding.ItemMonthlyOrderBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.ui.common.DataBoundListAdapter
import org.threeten.bp.Month

/*
 * Display list of items containing summary of transaction for each
 * day of the month.
 */
class DailySummaryListAdapter(
    executors: AppExecutors
) : DataBoundListAdapter<DailySummary, ItemMonthlyOrderBinding>(
    executors,
    OrderWithOrderItemsDiffCallback()
) {
    override fun createBinding(parent: ViewGroup): ItemMonthlyOrderBinding =
        ItemMonthlyOrderBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

    override fun bind(binding: ItemMonthlyOrderBinding, item: DailySummary) {
        binding.item = item
    }
}

private class OrderWithOrderItemsDiffCallback : DiffUtil.ItemCallback<DailySummary>() {
    override fun areItemsTheSame(oldItem: DailySummary, newItem: DailySummary) =
        oldItem.dayOfMonth == newItem.dayOfMonth

    override fun areContentsTheSame(oldItem: DailySummary, newItem: DailySummary) =
        oldItem == newItem
}

data class DailySummary(
    val dayOfMonth: Int,
    val month: Month,
    val totalRevenue: Long
)
