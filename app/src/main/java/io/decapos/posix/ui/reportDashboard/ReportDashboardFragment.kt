package io.decapos.posix.ui.reportDashboard

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.github.mikephil.charting.formatter.ValueFormatter
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentReportDashboardBinding
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.observe
import org.threeten.bp.Month
import javax.inject.Inject

class ReportDashboardFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ReportDashboardViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentReportDashboardBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewModel.monthlySummaryChartData.observe(this) {
            // Update chart when data changed
            binding.incomeChart.data = it
            binding.incomeChart.invalidate()
        }
        viewModel.productsTotalOrder.observe(this) {
            binding.popularProductChart.data = it
            binding.popularProductChart.invalidate()
        }

        binding = FragmentReportDashboardBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.cardviewIncome.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.report_dashboard_to_monthly_report)
        )
        styleChartMonthly()
        stylePiePopularProduct()
        return binding.root
    }

    private fun stylePiePopularProduct()  = binding.popularProductChart.apply {
        setEntryLabelColor(Color.BLACK)
        setTouchEnabled(false)
        description.isEnabled = false
        legend.isEnabled = false
        setUsePercentValues(true)
        extraBottomOffset = 10f
        extraTopOffset = 10f
    }

    private fun styleChartMonthly() = binding.incomeChart.apply {
        setNoDataTextColor(Color.WHITE)
        setTouchEnabled(false)
        legend.isEnabled = false
        description.isEnabled = false
        isHighlightPerTapEnabled = false
        setFitBars(true)
        axisRight.isEnabled = false
        axisLeft.granularity = 1f
        axisLeft.textColor = Color.WHITE
        axisLeft.spaceTop = 30f
        axisLeft.gridColor = getColor(context, R.color.whiteTransparent)
        axisLeft.setDrawLabels(false)
        axisLeft.setDrawAxisLine(false)
        xAxis.textColor = Color.WHITE
        xAxis.granularity = 1f
        xAxis.spaceMin = 0.3f
        xAxis.spaceMax = 0.3f
        xAxis.setDrawGridLines(false)
        xAxis.setDrawAxisLine(false)
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float) = Month.values()[value.toInt() - 1].toString()
        }
    }
}