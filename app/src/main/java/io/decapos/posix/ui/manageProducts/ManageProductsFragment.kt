package io.decapos.posix.ui.manageProducts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentManageProductsBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.observe
import io.decapos.posix.ui.common.ProductListAdapter
import javax.inject.Inject

class ManageProductsFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var mixpanel: MixpanelAPI

    private val viewModel by viewModels<ManageProductsViewModel> { viewModelFactory }

    private var adapter by autoCleared<ProductListAdapter>()

    private var binding by autoCleared<FragmentManageProductsBinding>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        adapter = ProductListAdapter(appExecutors) { navigateEditProduct(it.idProduct!!) }
        viewModel.products.observe(this) { adapter.submitList(it) }

        binding = FragmentManageProductsBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        binding.btnAddProducts.setOnClickListener { navigateNewProduct() }
        binding.recyclerAdminProduct.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        showDeleteUndoSnackbar(view)
    }

    private fun showDeleteUndoSnackbar(view: View) {
        if (viewModel.isDeleteUndoShown) return
        arguments?.let {
            ManageProductsFragmentArgs.fromBundle(it).deletedProduct
        }?.run {
            Snackbar.make(view, getString(R.string.snackbar_product_deleted, name), Snackbar.LENGTH_LONG)
                .setAction(R.string.undo) { viewModel.restoreProduct(this) }
                .show()
            viewModel.isDeleteUndoShown = true
        }
    }

    private fun navigateEditProduct(idProduct: Int) {
        val toEditProduct = ManageProductsFragmentDirections.manageProductsToEditProduct()
        toEditProduct.id = idProduct
        findNavController().navigate(toEditProduct)
    }

    private fun navigateNewProduct() {
        mixpanel.timeEvent("Add new product")
        findNavController().navigate(R.id.manage_products_to_new_product)
    }
}
