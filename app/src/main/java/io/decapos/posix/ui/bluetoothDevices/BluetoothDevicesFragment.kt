package io.decapos.posix.ui.bluetoothDevices

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothDevice
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentBluetoothDevicesBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import timber.log.Timber
import javax.inject.Inject

class BluetoothDevicesFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private val viewModel by viewModels<BluetoothDevicesViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentBluetoothDevicesBinding>()

    private var adapter by autoCleared<BluetoothDevicesAdapter>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentBluetoothDevicesBinding.inflate(inflater, container, false)

        adapter = BluetoothDevicesAdapter(appExecutors) { device ->
            viewModel.checkPrinter(device)
            if (device.bondState == BluetoothDevice.BOND_BONDED) findNavController().popBackStack()
            else {
                viewModel.deviceName = device.name
                viewModel.deviceAddress = device.address
            }
        }

        viewModel.bluetoothBondstate.observe(this) {
            if (it == BluetoothDevice.BOND_BONDED) {
                viewModel.addPrinter(viewModel.deviceName, viewModel.deviceAddress)
                findNavController().popBackStack()
            }
        }

        binding.recyclerBluetoothDevices.adapter = adapter
        viewModel.bluetoothDevices.observe(this) {
            adapter.submitList(it)
        }

        if (ContextCompat.checkSelfPermission(
                context!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity as Activity,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                0
            )
        }

        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            0 -> Timber.d("Permission Granted")
        }
    }
}