package io.decapos.posix.ui.reportContainer

import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import javax.inject.Inject

class ReportContainerViewModel @Inject constructor(
    orderRepository: OrderRepository
) : ViewModel() {
    val isOrderEmpty = orderRepository.isOrderEmpty()
}
