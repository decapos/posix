package io.decapos.posix.ui.cashierCheckout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentCashierCheckoutBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.Converter
import io.decapos.posix.shared.utils.attachCurrencyFormatter
import io.decapos.posix.shared.utils.attachPercentageFormatter
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.vo.CartItemsWithProduct
import javax.inject.Inject

class CashierCheckoutFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var mixpanel: MixpanelAPI

    private val viewModel by viewModels<CashierCheckoutViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentCashierCheckoutBinding>()

    private var checkoutProductListAdapter by autoCleared<CheckoutProductListAdapter>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCashierCheckoutBinding.inflate(inflater, container, false)
        checkoutProductListAdapter =
            CheckoutProductListAdapter(appExecutors) { viewModel.updateProduct(it) }

        viewModel.cartItems.observe(this, ::notifyCartUpdate)
        viewModel.userPaymentSuggestion.observe(this, ::setPaymentSuggestionChips)

        binding.textFieldUserPay.doOnTextChanged { text, _, _, _ ->
            if ( Converter.stringCurrencyToLong(text.toString()) != Converter.stringCurrencyToLong(
                    viewModel.selectedUserPaymentSuggestion.value
                )
            ) binding.paymentSuggestions.clearCheck()
        }
        binding.btnPay.setOnClickListener { initiateCheckout() }
        binding.textFieldUserPay.attachCurrencyFormatter(this)
        binding.editTextDiscount.attachPercentageFormatter(this)
        binding.toolbar.setupWithNavController(findNavController())
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.listCheckoutProduct.adapter = checkoutProductListAdapter
        binding.paymentSuggestions.setOnCheckedChangeListener(::updateSelectedUserPaymentSuggestion)
        return binding.root
    }

    private fun notifyCartUpdate(items: List<CartItemsWithProduct>) {
        if (items.isEmpty()) {
            mixpanel.track("Order cancelled")
            findNavController().navigate(R.id.cashier_checkout_to_cashier_select_product)
        } else {
            checkoutProductListAdapter.submitList(items)
        }
    }

    private fun initiateCheckout() {
        //TODO: this amount later will be dynamically based on user pay
        viewModel.pay()
        mixpanel.track("Order finished")
        findNavController().navigate(R.id.cashier_checkout_to_cashier_select_product)
    }

    private fun setPaymentSuggestionChips(suggestions: List<Long>) {
        binding.paymentSuggestions.removeAllViews()
        suggestions.forEach {
            val chip = Chip(context)
            chip.text = Converter.longToStringCurrency(it)
            chip.isCheckable = true
            binding.paymentSuggestions.addView(chip)
        }
    }

    private fun updateSelectedUserPaymentSuggestion(
        group: ChipGroup, checkedId: Int
    ) {
        if (checkedId == View.NO_ID) {
            viewModel.selectedUserPaymentSuggestion.value = ""
        } else {
            val checkedView = group.findViewById<Chip>(checkedId)
            viewModel.selectedUserPaymentSuggestion.value = checkedView.text.toString()
        }
    }
}