package io.decapos.posix.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import androidx.navigation.plusAssign
import androidx.navigation.ui.onNavDestinationSelected
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.shared.utils.setSelectedItemId
import io.decapos.posix.utils.KeepStateNavigator
import kotlinx.android.synthetic.main.fragment_container_main.view.*

/**
 *  This is the top level fragment for the app screen, this is where the bottomnav and the navhost that controls which
 *  section (admin, cashier, etc..) that is being shown lives.
 * */
class MainContainerFragment : Fragment(), Injectable {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_container_main, container, false)

        // Get reference to required views
        val navController = findNavController(view.findViewById(R.id.home_nav_host_fragment))
        val navHostFragment = childFragmentManager.findFragmentById(R.id.home_nav_host_fragment)

        // setup custom navigator and navController that will prevent child fragment from being destroyed when navigating
        // between sections.
        val navigator = KeepStateNavigator(
            context!!,
            navHostFragment!!.childFragmentManager,
            R.id.home_nav_host_fragment
        )
        navController.navigatorProvider += navigator
        navController.setGraph(R.navigation.home_nav_graph)

        // Bind bottomNav clicks to navigation
        view.bottom_navigation.setOnNavigationItemSelectedListener {
            // Prevent re-navigation to the same fragment when selecting the same item on bottomNav multiple times
            it.itemId != view.bottom_navigation.selectedItemId &&
                it.onNavDestinationSelected(navController) || super.onOptionsItemSelected(it)
        }

        view.bottom_navigation.setOnNavigationItemReselectedListener {
            view.findViewById<View>(R.id.section_nav_host_fragment)?.let {
                findNavController(it)
            }?.let {
                if (it.currentDestination?.id != it.graph.startDestination)
                    it.navigate(it.graph.startDestination)
            }
        }

        // When navigation navigate from other than bottomNav (eg. back button)
        // Update bottomNav
        navController.addOnDestinationChangedListener { _, destination, _ ->
            view.bottom_navigation.setSelectedItemId(destination.id, false)
        }
        return view
    }
}

