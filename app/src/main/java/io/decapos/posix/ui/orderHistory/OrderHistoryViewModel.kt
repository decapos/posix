package io.decapos.posix.ui.orderHistory

import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class OrderHistoryViewModel @Inject constructor(
    private val orderRepository: OrderRepository
): ViewModel() {
    fun restoreLastDeletedOrder() = CoroutineScope(IO).launch {
        orderRepository.undoLastDelete()
    }

    var undoHasBeenShown: Boolean = false

    val orders = orderRepository.getAllOrder()
}