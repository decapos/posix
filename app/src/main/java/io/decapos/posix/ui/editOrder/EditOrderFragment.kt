package io.decapos.posix.ui.editOrder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentEditOrderBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.attachCurrencyFormatter
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.observe
import javax.inject.Inject

class EditOrderFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var executors: AppExecutors

    private val viewModel by viewModels<EditOrderViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentEditOrderBinding>()

    private var adapter by autoCleared<OrderItemListAdapter>()

    private val args: EditOrderFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        adapter = OrderItemListAdapter(executors) { }
        viewModel.orderWithOrderItems.observe(this) { adapter.submitList(it?.orderItem) }
        viewModel.idOrder.value = args.idOrder

        binding = FragmentEditOrderBinding.inflate(inflater, container, false)
        binding.payAmount.attachCurrencyFormatter(viewLifecycleOwner)
        binding.totalPrice.attachCurrencyFormatter(viewLifecycleOwner)
        binding.btnDeleteOrder.setOnClickListener { deleteOrder(args.idOrder) }
        binding.btnSaveOrder.setOnClickListener { saveOrder() }
        binding.viewModel = viewModel
        binding.listOrderItem.adapter = adapter
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    private fun saveOrder() {
        viewModel.saveOrder()
        val direction = EditOrderFragmentDirections.actionEditOrderFragmentToOrderHistoryFragment()
        findNavController().navigate(direction)
    }

    private fun deleteOrder(idOrder: Int) {
        viewModel.deleteOrder(idOrder)
        val direction = EditOrderFragmentDirections.actionEditOrderFragmentToOrderHistoryFragment()
        direction.idOrder = idOrder
        findNavController().navigate(direction)
    }
}
