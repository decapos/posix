package io.decapos.posix.ui.community

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import io.decapos.posix.R
import io.decapos.posix.repository.FeedbackRepository
import io.decapos.posix.shared.utils.Event
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class CommunityViewModel @Inject constructor(
    private val feedbackRepo: FeedbackRepository
) : ViewModel() {
    val feedbackMessage = MutableLiveData("")
    val isSendingMessage = MutableLiveData(false)
    val feedbackPostedEvent = MutableLiveData<Event<Int>>()
    val isFeedbackEmpty = feedbackMessage.map {
        it.isEmpty()
    }

    @Suppress("UNUSED_PARAMETER")
    fun sendFeedback(view: View) {
        isSendingMessage.value = true
        feedbackMessage.value?.let {
            if (feedbackMessage.value == "") return
            feedbackRepo.sendFeedback(it)
        }?.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                feedbackMessage.postValue("")
                feedbackPostedEvent.value = Event(R.string.feedback_sent_snack)
                isSendingMessage.value = false
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                feedbackPostedEvent.value = Event(R.string.feedback_failed_sent_snack)
                isSendingMessage.value = false
            }
        })
    }
}