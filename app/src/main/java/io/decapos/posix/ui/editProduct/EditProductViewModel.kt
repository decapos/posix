package io.decapos.posix.ui.editProduct

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import io.decapos.posix.repository.ProductRepository
import io.decapos.posix.shared.utils.Converter
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class EditProductViewModel @Inject constructor(
    private val productRepo: ProductRepository
) : ViewModel() {
    // TODO: Might be able to turn this var into val
    //  and simplify val product to use extension function
    val id = MutableLiveData<Int>()

    val errorMsg = MutableLiveData<String>()

    var imageUri: String? = ""

    val product = id.switchMap {
        productRepo.getProductById(it)
    }

    val name = MediatorLiveData<String>().apply {
        addSource(product) { value = it.name }
    }

    val price = MediatorLiveData<String>().apply {
        addSource(product) { value = Converter.longToStringCurrency(it.price) }
    }

    fun saveProductChanges() = viewModelScope.launch {
        val newName = name.value ?: ""
        val newPrice = price.value ?: ""

        if (newName == "" || newPrice == "") {
            // TODO: Move this to strings.xml
            errorMsg.value = "Field tidak boleh kosong"
        } else {
            val finalPrice = Converter.stringCurrencyToLong(newPrice)
            withContext(IO) {
                id.value?.let {
                    productRepo.updatePriceById(it, finalPrice)
                    productRepo.updateNameById(it, newName)
                    productRepo.updateImageById(it, imageUri ?: "")
                }
            }
            errorMsg.value = null
        }
    }

    fun deleteProduct() = viewModelScope.launch(IO) {
        id.value?.let {
            productRepo.deleteProduct(it)
        }
    }
}
