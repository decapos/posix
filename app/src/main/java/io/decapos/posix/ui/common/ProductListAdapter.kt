package io.decapos.posix.ui.common

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.squareup.picasso.Picasso
import io.decapos.posix.R
import io.decapos.posix.databinding.ItemProductBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.vo.Product

/**
 *  Adapter for showing list of products.
 * */
class ProductListAdapter(
    executors: AppExecutors,
    val callback: ((Product) -> Unit)?
) : DataBoundListAdapter<Product, ItemProductBinding>(
    executors, ProductDiffCallback()
) {
    override fun createBinding(parent: ViewGroup): ItemProductBinding {
        val binding = ItemProductBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        binding.containerProduct.setOnClickListener {
            binding.product?.let { product -> callback?.invoke(product) }
        }
        return binding
    }

    override fun bind(binding: ItemProductBinding, item: Product) {
        //TODO: having delayed when load image using this method, find other way
        Picasso.get()
            .load(Uri.parse(item.imagePath))
            .placeholder(R.drawable.image_placeholder_product)
            .into(binding.thumbnailProduct)
        binding.product = item
    }
}

private class ProductDiffCallback : DiffUtil.ItemCallback<Product>() {
    override fun areContentsTheSame(old: Product, new: Product) = old == new

    override fun areItemsTheSame(old: Product, new: Product) =
        old.idProduct == new.idProduct
}
