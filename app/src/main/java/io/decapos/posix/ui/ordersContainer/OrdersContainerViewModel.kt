package io.decapos.posix.ui.ordersContainer

import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import io.decapos.posix.repository.OrderRepository
import javax.inject.Inject

class OrdersContainerViewModel @Inject constructor(
    orderRepository: OrderRepository
) : ViewModel() {
    val orders = orderRepository.getAllOrder()
    val isOrderEmpty = Transformations.map(orders) { it.isEmpty() }
}