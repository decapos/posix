package io.decapos.posix.ui.reportContainer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import kotlinx.android.synthetic.main.fragment_container_report.view.*
import javax.inject.Inject

class ReportContainerFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ReportContainerViewModel> { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_container_report, container, false)
        // toolbar.setupWithNavController(navController)
        viewModel.isOrderEmpty.observe(this) {
            view.empty_switcher.displayedChild = if (it) 0 else 1
            if (it != null) view.empty_switcher.visibility = View.VISIBLE
        }
        return view
    }
}

