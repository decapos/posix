package io.decapos.posix.ui.community

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.google.android.material.snackbar.Snackbar
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentCommunityBinding
import javax.inject.Inject

class CommunityFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<CommunityViewModel> { viewModelFactory }

    private lateinit var binding: FragmentCommunityBinding

    private val spectrumUrl =
        "https://spectrum.chat/deca"
    private val roadmapUrl =
        "https://www.notion.so/decafeedback/Deca-Feedback-Channel-834ee1e04ea84945aa53e125495a2eb5"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentCommunityBinding.inflate(inflater, container, false)

        viewModel.feedbackPostedEvent.observe(this) { event ->
            event.getContentIfNotHandled()?.let {
                Snackbar.make(binding.root, getString(it), Snackbar.LENGTH_LONG).show()
            }
        }
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.roadmapLink.setOnClickListener {
            openLink(roadmapUrl)
        }
        binding.slackInvite.setOnClickListener {
            openLink(spectrumUrl)
        }
        return binding.root
    }

    private fun openLink(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }
}