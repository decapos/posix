package io.decapos.posix.ui.managePrinters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.decapos.posix.databinding.ItemPrinterBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.ui.common.DataBoundListAdapter
import io.decapos.posix.vo.Printer

class ListPrinterAdapter(
    appExecutors: AppExecutors, val callback: ((Printer, Int) -> Unit)?
) : DataBoundListAdapter<Pair<Printer, Boolean>, ItemPrinterBinding>(
        appExecutors, PrinterDiffUtil()
){
    companion object {
        const val TEST_PRINT = 1
        const val DELETE_PRINTER = 2
        const val SET_DEFAULT = 3
    }

    override fun createBinding(parent: ViewGroup): ItemPrinterBinding {
        val binding = ItemPrinterBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        binding.btnTestPrinter.setOnClickListener {
            binding.printer?.let { printer ->
                callback?.invoke(printer, TEST_PRINT)
            }
        }

        binding.imageSettings.setOnClickListener {
            binding.printer?.let { printer ->
                callback?.invoke(printer, DELETE_PRINTER)
            }
        }

        binding.btnSetDefault.setOnClickListener {
            binding.printer?.let { printer ->
                callback?.invoke(printer, SET_DEFAULT)
            }
        }

        return binding
    }

    override fun bind(binding: ItemPrinterBinding, item: Pair<Printer, Boolean>) {
        binding.printer = item.first
        binding.isDefault = item.second
    }
}

private class PrinterDiffUtil : DiffUtil.ItemCallback<Pair<Printer, Boolean>>() {
    override fun areItemsTheSame(oldItem: Pair<Printer, Boolean>, newItem: Pair<Printer, Boolean>) =
        oldItem.first.idPrinter == newItem.first.idPrinter

    override fun areContentsTheSame(oldItem: Pair<Printer, Boolean>, newItem: Pair<Printer, Boolean>) =
        oldItem == newItem
}
