package io.decapos.posix.ui.monthlyReport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentMonthlyReportBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import io.decapos.posix.shared.utils.observe
import javax.inject.Inject

class MonthlyReportFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    private val viewModel by viewModels<MonthlyReportViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentMonthlyReportBinding>()

    private var adapter by autoCleared<MonthlySummaryListAdapter>()

    // TODO: Implement monthly summary details on click
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        adapter = MonthlySummaryListAdapter(appExecutors) {
            navigateMonthlyBreakdown(it.yearMonth.toString())
        }
        viewModel.ordersByMonth.observe(this) { adapter.submitList(it) }

        binding = FragmentMonthlyReportBinding.inflate(inflater, container, false)
        binding.toolbar.setupWithNavController(findNavController())
        binding.recyclerMonthlySummary.adapter = adapter
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    private fun navigateMonthlyBreakdown(yearMonth: String) {
        val monthlyBreakdownDirection =
            MonthlyReportFragmentDirections.monthlyReportToMonthlyBreakdown(yearMonth)
        findNavController().navigate(monthlyBreakdownDirection)
    }
}