package io.decapos.posix.ui.managePromos

import androidx.lifecycle.ViewModel
import javax.inject.Inject

@Suppress("EmptyClassBlock")
class ManagePromosViewModel @Inject constructor() : ViewModel()
