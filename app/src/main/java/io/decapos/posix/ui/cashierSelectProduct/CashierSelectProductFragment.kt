package io.decapos.posix.ui.cashierSelectProduct

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import com.google.android.material.snackbar.Snackbar
import com.mixpanel.android.mpmetrics.MixpanelAPI
import io.decapos.posix.R
import io.decapos.posix.dagger.Injectable
import io.decapos.posix.databinding.FragmentCashierSelectProductBinding
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.shared.utils.autoCleared
import javax.inject.Inject

class CashierSelectProductFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var appExecutors: AppExecutors

    @Inject
    lateinit var mixpanel: MixpanelAPI

    private val viewModel by viewModels<CashierSelectProductViewModel> { viewModelFactory }

    private var binding by autoCleared<FragmentCashierSelectProductBinding>()

    private var adapter by autoCleared<CashierProductListAdapter>()

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        adapter = CashierProductListAdapter(appExecutors) { product, cartDetail ->
            viewModel.addProductToCart(product, cartDetail)
        }
        viewModel.products.observe(this) {
            val cartItems = viewModel.cartItems.value
            if (cartItems?.size == 1 && cartItems[0].cartDetail.quantity == 1) {
                mixpanel.timeEvent("Order finished")
                mixpanel.track("Order start")
            }
            adapter.submitList(it)
        }

        binding = FragmentCashierSelectProductBinding.inflate(inflater, container, false)
        binding.btnCheckout.setOnClickListener {
            mixpanel.track("Order checkout")
            findNavController().navigate(R.id.cashier_select_product_to_cashier_checkout)
        }
        // Bind the clear search drawable
        binding.edittextSearch.setOnTouchListener { _, event -> calculateIsClearSearchClicked(event) }
        binding.viewModel = viewModel
        binding.recyclerChosenProduct.adapter = adapter
        // Disables item change animation to speed up item click responsiveness
        binding.recyclerChosenProduct.itemAnimator = DefaultItemAnimator().apply { supportsChangeAnimations = false }
        binding.lifecycleOwner = viewLifecycleOwner
        binding.iconClearCart.setOnClickListener { clearCart() }
        return binding.root
    }

    private fun clearCart() {
        viewModel.handleClearCart()
        Snackbar.make(view!!, R.string.snackbar_cart_reset, Snackbar.LENGTH_SHORT)
            .setAction(R.string.undo) { viewModel.restoreCart() }
            .show()
    }

    private fun calculateIsClearSearchClicked(event: MotionEvent): Boolean {
        val drawableRight = 2
        if (event.action == MotionEvent.ACTION_UP) {
            binding.edittextSearch.compoundDrawables[drawableRight]?.let {
                if (event.rawX >= (binding.edittextSearch.right - it.bounds.width())) {
                    binding.edittextSearch.setText("")
                    return true
                }
            }
        }
        return false
    }
}