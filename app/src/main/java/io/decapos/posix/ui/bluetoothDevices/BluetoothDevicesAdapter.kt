package io.decapos.posix.ui.bluetoothDevices

import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import io.decapos.posix.shared.utils.AppExecutors
import io.decapos.posix.databinding.ItemBluetoothDeviceBinding
import io.decapos.posix.ui.common.DataBoundListAdapter

class BluetoothDevicesAdapter(
    appExecutors: AppExecutors,
    val callback: ((BluetoothDevice) -> Unit)?
) : DataBoundListAdapter<BluetoothDevice, ItemBluetoothDeviceBinding>(
    appExecutors, DeviceDiffUtil()
) {
    override fun createBinding(parent: ViewGroup): ItemBluetoothDeviceBinding {
        val binding = ItemBluetoothDeviceBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        binding.containerBluetooth.setOnClickListener {
            binding.device?.let { device ->
                callback?.invoke(device)
            }
        }
        return binding
    }

    override fun bind(binding: ItemBluetoothDeviceBinding, item: BluetoothDevice) {
        binding.device = item
    }
}

private class DeviceDiffUtil : DiffUtil.ItemCallback<BluetoothDevice>() {
    override fun areItemsTheSame(oldItem: BluetoothDevice, newItem: BluetoothDevice) =
        oldItem == newItem

    override fun areContentsTheSame(oldItem: BluetoothDevice, newItem: BluetoothDevice) =
        oldItem == newItem
}