package io.decapos.posix.utils

import io.decapos.posix.repository.PreferenceRepository
import io.decapos.posix.shared.utils.Converter
import io.decapos.posix.vo.OrdersWithOrderItems
import java.io.OutputStream
import javax.inject.Inject

class PrinterHelper @Inject constructor(
    outputStream: OutputStream,
    preferenceRepository: PreferenceRepository
) {
    private val os: OutputStream = outputStream
    private val pr: PreferenceRepository = preferenceRepository

    val ALIGN_CENTER = byteArrayOf(0x1B, 0x61, 1)
    val ALIGN_LEFT = byteArrayOf(0x1B, 0x61, 0)
    val ALIGN_RIGHT = byteArrayOf(0x1B, 0x61, 2)
    val NORMAL_BOLD = byteArrayOf(0x1B,0x21,0x08)
    val LARGE_BOLD = byteArrayOf(0x1B,0x21,0x10)
    val NORMAL_REGULER = byteArrayOf(27, 33, 0)

    fun printNewline(line: Int) {
        var i = 0
        while (i < line) {
            os.write(("\n").toByteArray())
            i++
        }
    }

    private fun printCustom(msg: String, styles: Array<ByteArray>) {
        styles.forEach { os.write(it) }
        os.write(msg.toByteArray())
    }

    fun printTest() {
        printCustom("print OK", arrayOf(ALIGN_LEFT, NORMAL_REGULER))
        printNewline(3)
        os.flush()
    }

    fun printBill(item: OrdersWithOrderItems, pay: Long) {
        var qty = 0
        printNewline(2)
        if (pr.getCompanyName().isNotBlank()) {
            printCustom(pr.getCompanyName(), arrayOf(ALIGN_CENTER, LARGE_BOLD))
        }
        if (pr.getCompanyAddress().isNotBlank()) {
            printNewline(1)
            printCustom(pr.getCompanyAddress(), arrayOf(ALIGN_CENTER, NORMAL_REGULER))
        }
        if (pr.getCompanyPhone().isNotBlank()) {
            printNewline(1)
            printCustom(pr.getCompanyPhone(), arrayOf(ALIGN_CENTER, NORMAL_REGULER))
        }
        printNewline(2)
        printCustom(
            String.format(
                "Tanggal: %1\$-10s %2\$12s",
                Converter.instantToStringDate(item.order.createdAt),
                Converter.instantToStringTime(item.order.createdAt)
            ), arrayOf(ALIGN_CENTER, NORMAL_REGULER)
        )
        printNewline(1)
        printCustom(String.format("No: #%d", item.order.idOrder), arrayOf(ALIGN_LEFT, NORMAL_REGULER))
        printNewline(1)
        printCustom("--------------------------------", arrayOf(ALIGN_CENTER, NORMAL_REGULER))
        item.orderItem.forEach {
            printCustom(it.name, arrayOf(ALIGN_LEFT, NORMAL_REGULER))
            printNewline(1)
            printCustom(
                String.format(
                    "%1\$-10s %2\$3s %3\$6s %4\$10s",
                    Converter.longToStringCurrency(it.price),
                    "x" + it.quantity, "=",
                    Converter.longToStringCurrency(it.totalPrice)
                ), arrayOf(ALIGN_CENTER)
            )
            printNewline(1)
            qty += it.quantity
        }
        printCustom("--------------------------------", arrayOf(ALIGN_CENTER, NORMAL_REGULER))
        printNewline(1)
        printCustom(
            String.format(
                "%1\$-10s %2\$3s %3\$6s %4\$10s",
                "Total", qty, "=",
                Converter.longToStringCurrency(item.order.actualPayAmount)
            ), arrayOf(ALIGN_CENTER, NORMAL_REGULER)
        )
        printNewline(1)
        printCustom(
            String.format(
                "%1\$-10s %2\$10s %3\$10s",
                "Tunai", "=", Converter.longToStringCurrency(pay)
            ), arrayOf(ALIGN_CENTER, NORMAL_REGULER)
        )
        printNewline(1)
        printCustom("--------------------------------", arrayOf(ALIGN_CENTER, NORMAL_REGULER))
        printNewline(1)
        printCustom(
            String.format(
                "%1\$-10s %2\$10s %3\$10s", "Kembali", "=",
                Converter.longToStringCurrency(
                    pay - item.order.actualPayAmount
                )
            ), arrayOf(ALIGN_CENTER, NORMAL_REGULER)
        )
        if (pr.getNotes().isNotBlank()) {
            printNewline(2)
            printCustom(pr.getNotes(), arrayOf(ALIGN_CENTER, NORMAL_REGULER))
        }
        printNewline(5)
        os.flush()
    }
}