package io.decapos.posix.utils

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.FileProvider
import io.decapos.posix.R
import io.decapos.posix.shared.utils.OpenForTesting
import java.io.File
import javax.inject.Inject

/* TODO; Fix this file
 *      this file is currently a mess, some problems are:
 *      1. To big as helper
 *      2. It does a very specific function as a helper (eg. make notification
 *          specifically for exporting data.
 *
 * Possible solutions:
 * 1. Extract into useCase
 * 2. ...
 */
@OpenForTesting
class NotificationHelper @Inject constructor(
    private val app: Application
) : ContextWrapper(app.baseContext) {
    final val CHANNEL_NAME = "report"

    //notification message
    final val NOTIF_MESSAGE_TITLE_EXPORTING = "Exporting.."
    final val NOTIF_MESSAGE_TITLE_EXPORTED = "Export completed"

    //notification id
    val NOTIF_ID_EXPORT_CSV = 1001

    private val manager: NotificationManager by lazy {
        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    init {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(PRIMARY_CHANNEL, CHANNEL_NAME, importance)
            // Register the channel with the system
            manager.createNotificationChannel(channel)
        }
    }

    fun getNotificationPrimaryChannel(title: String, body: String): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext,
            PRIMARY_CHANNEL
        )
            .setContentTitle(title)
            .setContentText(body)
            .setSmallIcon(R.mipmap.ic_notif_icon)
            .setAutoCancel(true)
    }

    fun notify(id: Int, notification: NotificationCompat.Builder) {
        manager.notify(id, notification.build())
    }

    fun notifyExport(filePath: String) {
        val ctx = app.baseContext
        // letting user now their saved file location by notification

        val fp = FileProvider.getUriForFile(ctx, ctx.applicationContext.packageName + ".provider", File(filePath))
        val intent = Intent(Intent.ACTION_VIEW, fp).apply {
            flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        }

        val mBuilder = getNotificationPrimaryChannel(NOTIF_MESSAGE_TITLE_EXPORTING, "Exporting to $filePath")
        val pendingIntent: PendingIntent = PendingIntent.getActivity(ctx, 0, intent, 0)
        mBuilder.setProgress(0, 0, false)
            .setContentTitle(NOTIF_MESSAGE_TITLE_EXPORTED)
            .setContentIntent(pendingIntent)
            .setContentText(filePath)
        notify(NOTIF_ID_EXPORT_CSV, mBuilder)
    }

    companion object {
        const val PRIMARY_CHANNEL = "default"
    }
}