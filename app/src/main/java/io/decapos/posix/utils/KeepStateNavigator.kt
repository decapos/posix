package io.decapos.posix.utils

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.FragmentManager
import androidx.navigation.NavDestination
import androidx.navigation.NavOptions
import androidx.navigation.Navigator
import androidx.navigation.fragment.FragmentNavigator

/**
 * Courtesy of https://github.com/STAR-ZERO/navigation-keep-fragment-sample
 *
 * Due to a bug on android navigation, it is currently impossible to automatically
 * saveProductChanges fragment state using the bottomNavigationView after navigating away because
 * fragment is destroyed right away after user navigate away (see: https://issuetracker.google.com/issues/80029773)
 *
 * Current solution is to override the default Navigator to and replace it with one that
 * prevent the fragment from being destroyed right away, by using KeepStateNavigator below.
 *
 * */
@Navigator.Name("keep_state_fragment") // `keep_state_fragment` is used in navigation XMLs
class KeepStateNavigator(
    private val context: Context,
    private val manager: FragmentManager,
    private val containerId: Int
) : FragmentNavigator(context, manager, containerId) {

    override fun navigate(
        destination: Destination,
        args: Bundle?,
        navOptions: NavOptions?,
        navigatorExtras: Navigator.Extras?
    ): NavDestination? {
        val tag = destination.id.toString()
        val transaction = manager.beginTransaction()

        val currentFragment = manager.primaryNavigationFragment
        if (currentFragment != null) {
            transaction.detach(currentFragment)
        }

        var fragment = manager.findFragmentByTag(tag)
        if (fragment == null) {
            val className = destination.className
            fragment = manager.fragmentFactory.instantiate(context.classLoader, className)
            transaction.add(containerId, fragment, tag)
        } else {
            transaction.attach(fragment)
        }

        transaction.setPrimaryNavigationFragment(fragment)
        transaction.setReorderingAllowed(true)
        transaction.commit()

        return destination
    }
}