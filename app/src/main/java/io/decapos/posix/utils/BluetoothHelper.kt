package io.decapos.posix.utils

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import io.decapos.posix.repository.OrderRepository
import io.decapos.posix.repository.PreferenceRepository
import io.decapos.posix.repository.PrinterRepository
import timber.log.Timber
import java.io.IOException
import java.util.UUID
import javax.inject.Inject

// TODO: Class called bluetoothHelper, but handles querying order and sending it to printer?
//  This class responsibility needs to be defined more clearly.
class BluetoothHelper @Inject constructor(
    private val printerRepo: PrinterRepository,
    private val preferenceRepo: PreferenceRepository,
    private val orderRepo: OrderRepository
) {
    // TODO: Magic Number, need to be explained maybe extracted into a constant with a sensible name.
    private val applicationUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
    private var socket: BluetoothSocket? = null
    private var bluetoothDevice: BluetoothDevice? = null

    private lateinit var printerHelper: PrinterHelper

    // TODO: Internal state? is it necessary? this increase complexity, can it be removed?
    val bluetoothDevices = printerRepo.getPairedPrinter()

    fun testPrint(address: String) {
        bluetoothDevices.map {
            if (it.address == address) {
                openConnection(it)
                printerHelper = PrinterHelper(socket!!.outputStream, preferenceRepo)
                printerHelper.printTest()
                closeConnection(socket!!)
            }
        }
    }

    fun printBill(idOrder: Int, pay: Long) {
        if (bluetoothDevices.isEmpty()) return //TODO: right now to check if bluetooth is enabled. need better solution
        bluetoothDevice = printerRepo.getDefaultPrinter(preferenceRepo.getDefaultPrinterAddress())
        if (bluetoothDevice == null) return

        val order = orderRepo.getOrderWithOrderItems(idOrder)
        openConnection(bluetoothDevice!!)
        printerHelper = PrinterHelper(socket!!.outputStream, preferenceRepo)
        printerHelper.printBill(order, pay)
        closeConnection(socket!!)
    }

    fun setPrinterDefault(address: String) {
        preferenceRepo.setDefaultPrinterAddress(address)
    }

    private fun openConnection(device: BluetoothDevice) {
        try {
            socket = device.createRfcommSocketToServiceRecord(applicationUUID)
            socket!!.connect()
            Timber.d("Connection open")
        } catch (eConnectException: IOException) {
            closeConnection(this.socket!!)
            return
        }
    }

    private fun closeConnection(socket: BluetoothSocket) {
        try {
            socket.close()
            Timber.d("Socket closed")
        } catch (ex: IOException) {
            Timber.d("Couldn't close socket")
        }
    }
}