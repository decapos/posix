package io.decapos.posix

import android.app.Activity
import android.app.Application
import androidx.core.content.edit
import androidx.preference.PreferenceManager
import com.bugsnag.android.BreadcrumbType
import com.bugsnag.android.Bugsnag
import com.jakewharton.threetenabp.AndroidThreeTen
import com.mixpanel.android.mpmetrics.MixpanelAPI
import com.survicate.surveys.Survicate
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.decapos.posix.dagger.AppInjector
import io.decapos.posix.shared.utils.ReleaseTree
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor
import org.json.JSONObject
import timber.log.Timber
import java.util.UUID
import javax.inject.Inject

// Class is open so we can extend it in debug
open class PosixApp : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var mixpanel: MixpanelAPI

    override fun activityInjector() = dispatchingActivityInjector

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this, provideInterceptor())
        Survicate.init(this)
        Bugsnag.init(this)
        AndroidThreeTen.init(this)

        if (!BuildConfig.DEBUG) {
            val pref = PreferenceManager.getDefaultSharedPreferences(this)
            val id = pref.getString("UserId", UUID.randomUUID().toString())
            pref.edit { putString("UserId", id) }

            mixpanel.identify(id)
            Bugsnag.setUserId(id)
            Timber.plant(ReleaseTree())
        }
    }

    // Provide logging interceptor on prod, (overridden in debug with Stetho)
    protected open fun provideInterceptor(): Interceptor = HttpLoggingInterceptor {
        val breadcrumb = mutableMapOf<String, String>()
        breadcrumb["data"] = it
        mixpanel.track(it, JSONObject(breadcrumb as Map<*, *>))
        Bugsnag.leaveBreadcrumb("api call", BreadcrumbType.REQUEST, breadcrumb)
    }
}
