package io.decapos.posix

import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import dagger.android.HasActivityInjector
import okhttp3.Interceptor
import timber.log.Timber

class PosixAppDebug : PosixApp(), HasActivityInjector {
    override fun onCreate() {
        super.onCreate()
        // Debug tooling
        Stetho.initializeWithDefaults(this)
        Timber.plant(Timber.DebugTree())
    }

    override fun provideInterceptor(): Interceptor = StethoInterceptor()
}
