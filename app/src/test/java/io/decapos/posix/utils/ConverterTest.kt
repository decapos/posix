package io.decapos.posix.utils

import com.google.common.truth.Truth.assertThat
import io.decapos.posix.shared.utils.Converter
import org.junit.Test
import org.threeten.bp.Instant
import org.threeten.bp.YearMonth

class ConverterTest {
    @Test
    fun givenInstant_whenConvertedToString_thenFormatLocaleID() {
        val instant = Instant.ofEpochMilli(0)
        val string = Converter.instantToString(instant)
        assertThat(string).isEqualTo("Kamis, 1970 Januari 01")
    }

    @Test
    fun givenLong_whenConvertedToCurrency_thenFormatIDRLocaleID() {
        val number = 123543L
        assertThat(Converter.longToStringCurrency(number)).isEqualTo("Rp123.543")
    }

    @Test
    fun givenYearMonth_whenConvertedToString_thenShowCorrectYearAndMonth() {
        val yearMonth = YearMonth.of(2012, 12)
        val string = Converter.yearMonthToString(yearMonth)
        assertThat(string).isEqualTo("December 2012")
    }
}