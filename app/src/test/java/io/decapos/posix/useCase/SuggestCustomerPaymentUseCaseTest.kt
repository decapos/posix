package io.decapos.posix.useCase

import com.google.common.truth.Truth.assertThat
import org.junit.Before
import org.junit.Test

class SuggestCustomerPaymentUseCaseTest {
    lateinit var useCase: SuggestCustomerPaymentUseCase

    @Before
    fun setupUseCase() {
        useCase = SuggestCustomerPaymentUseCase()
    }

    @Test
    fun resultDoesNotContainOriginalPrice() {
        val price = 180_000L
        val result = useCase(price)
        assertThat(result).doesNotContain(price)
    }

    @Test
    fun allSuggestionIsAbovePrice() {
        val price = 180_000L
        val result = useCase(price)
        result.forEach {
            assertThat(it).isGreaterThan(price)
        }
    }

    @Test
    fun suggestionIsNeverEmpty() {
        val prices = arrayOf<Long>(
            23_000,
            1_000,
            45_500,
            125_000,
            800_000,
            1_000_000,
            900,
            800,
            14_000,
            77_500
        )
        prices.forEach {
            val result = useCase(it)
            assertThat(result).isNotEmpty()
        }
    }
}