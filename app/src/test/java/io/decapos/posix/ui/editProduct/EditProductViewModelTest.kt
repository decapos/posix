package io.decapos.posix.ui.editProduct

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.decapos.posix.repository.ProductRepository
import io.decapos.posix.utils.mock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import java.util.concurrent.Executors.newSingleThreadExecutor

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class EditProductViewModelTest {

    // replaces dispatcher Main (which is unavailable on junit)
    private val mainThreadSurrogate = newSingleThreadExecutor().asCoroutineDispatcher()

    private val repository = mock(ProductRepository::class.java)

    private val viewModel = EditProductViewModel(repository)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun givenIdChanged_whenNameObserved_correctProductIdQueried() {
        val id = 1
        viewModel.id.value = 1
        viewModel.name.observeForever(mock())
        verify(repository).getProductById(id)
    }

    @Test
    fun givenNameAndPrice_whenSaved_CorrectNameAndPriceSaved() {
        val id = 1
        val name = "Loco"
        val price = "Rp221.122"
        viewModel.id.value = id
        viewModel.name.value = name
        viewModel.price.value = price
        runBlocking {
            viewModel.saveProductChanges().join()
            verify(repository).updateNameById(id, name)
            verify(repository).updatePriceById(id, 221122)
        }
    }

    @Test
    fun givenEmptyName_whenSaved_doNothing() {
        val id = 1
        val name = ""
        val price = "121.122"
        viewModel.id.value = id
        viewModel.name.value = name
        viewModel.price.value = price
        runBlocking {
            viewModel.saveProductChanges().join()
            verify(repository, never()).updateNameById(id, name)
            verify(repository, never()).updatePriceById(id, price.replace(".", "").toLong())
        }
    }

    @Test
    fun givenEmptyPrice_whenSaved_doNothing() {
        val id = 1
        val name = "Loki"
        val price = ""
        viewModel.id.value = id
        viewModel.name.value = name
        viewModel.price.value = price
        runBlocking {
            viewModel.saveProductChanges().join()
            verify(repository, never()).updateNameById(id, name)
            verify(repository, never()).updatePriceById(id, 0L)
        }
    }
}