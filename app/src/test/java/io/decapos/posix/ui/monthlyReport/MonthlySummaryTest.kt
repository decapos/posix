package io.decapos.posix.ui.monthlyReport

import com.google.common.truth.Truth.assertThat
import io.decapos.posix.vo.MonthlySummary
import org.junit.Test
import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId

class MonthlySummaryTest {
    @Test
    fun givenOrders_whenChangedToMonthlySummary_thenRevenueCorrect() {
        val orders = io.decapos.posix.test_shared.TestUtils.createOrders(10, 1000)
        val summaries = MonthlySummary(orders, null)
        assertThat(summaries.revenue).isEqualTo(10 * 1000)
    }

    @Test
    fun givenOrders_whenChangedToMonthlySummary_yearMonthCorrect() {
        val orders = io.decapos.posix.test_shared.TestUtils.createOrders(10, 1000)
        val summaries = MonthlySummary(orders, null)
        assertThat(summaries.yearMonth).isEqualTo(YearMonth.from(orders[0].createdAt.atZone(ZoneId.systemDefault())))
    }
}