package io.decapos.posix.ui.community

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.decapos.posix.repository.FeedbackRepository
import io.decapos.posix.utils.mock
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify

@RunWith(JUnit4::class)
class CommunityViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private val repository = mock(FeedbackRepository::class.java)
    private val viewModel = CommunityViewModel(repository)

    @Test
    fun givenEmptyFeedback_whenSendTriggered_doNothing() {
        val message = ""
        viewModel.feedbackMessage.value = message
        viewModel.sendFeedback(mock())
        verify(repository, never()).sendFeedback(message)
    }

    @Test
    fun givenAFeedback_whenSendTriggered_doNothing() {
        val message = "You sucks"
        viewModel.feedbackMessage.value = message
        viewModel.sendFeedback(mock())
        verify(repository).sendFeedback(message)
    }
}