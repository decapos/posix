package io.decapos.posix.ui.newProduct

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.decapos.posix.repository.ProductRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import java.util.concurrent.Executors.newSingleThreadExecutor

@RunWith(JUnit4::class)
@ExperimentalCoroutinesApi
class NewProductViewModelTest {
    // replaces dispatcher Main (which is unavailable on junit)
    private val mainThreadSurrogate = newSingleThreadExecutor().asCoroutineDispatcher()

    private val repository = mock(ProductRepository::class.java)

    private val viewModel = NewProductViewModel(repository)

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun givenNameAndPrice_whenSaved_CorrectNameAndPriceSaved() {
        val name = "Loco"
        val price = "Rp121.122"
        viewModel.name.value = name
        viewModel.price.value = price
        runBlocking {
            viewModel.saveNewProduct().join()
            verify(repository).insert(name, 121122, "")
        }
    }

    @Test
    fun givenEmptyName_whenSaved_doNothing() {
        val name = ""
        val price = "Rp121.122"
        viewModel.name.value = name
        viewModel.price.value = price
        runBlocking {
            viewModel.saveNewProduct().join()
            verify(repository, never()).insert(name, 121122, "")
        }
    }

    @Test
    fun givenEmptyPrice_whenSaved_doNothing() {
        val name = "Juma"
        val price = ""
        viewModel.name.value = name
        viewModel.price.value = price
        runBlocking {
            viewModel.saveNewProduct().join()
            verify(repository, never()).insert(name, 0L, "")
        }
    }
}