package io.decapos.posix.e2e

import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.action.ViewActions.typeText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withHint
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withSubstring
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import io.decapos.posix.MainActivity
import io.decapos.posix.R
import io.decapos.posix.repository.dagger.DaggerRepositoryComponent
import io.decapos.posix.vo.Product
import kotlinx.coroutines.runBlocking
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class ManageProductTest {
    private val repositoryComponent =
        DaggerRepositoryComponent.builder().application(getApplicationContext())
            .networkInterceptor(HttpLoggingInterceptor {}).build()

    @get:Rule
    val activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    // espresso doesn't know when dataBinding is idling, so we need to tell it when with this.
    @get:Rule
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    @Before
    fun resetDatabase() {
        // make sure all test starts on the same empty state
        repositoryComponent.generalRepository.clearAllData()
    }

    @Test
    fun addNewProduct() {
        activityRule.launchActivity(null)

        // Go to admin page
        onView(withId(R.id.admin_container))
            .perform(click())

        onView(withText(R.string.kelola_produk))
            .perform(click())

        onView(withId(R.id.btn_add_products))
            .perform(click())

        // Input new product detail;
        onView(withHint(R.string.hint_product_name))
            .perform(typeText("Cyberpunk 2077"))

        onView(withHint(R.string.hint_price))
            .perform(typeText("854347"))
            .check(matches(withText("Rp854.347"))) // make sure formatting is right

        onView(withId(R.id.btn_add_product))
            .perform(click())

        onView(withText("Cyberpunk 2077"))
            .check(matches(isDisplayed()))

        // check is displayed on cashier
        onView(withId(R.id.cashier_container))
            .perform(click())

        onView(withText("Cyberpunk 2077"))
            .check(matches(isDisplayed()))
    }

    @Test
    fun editExistingProduct() {
        runBlocking {
            repositoryComponent.productRepository.insert(Product(name = "BFR", price = 1000))
        }

        activityRule.launchActivity(null)

        onView(withId(R.id.admin_container))
            .perform(click())

        onView(withText(R.string.kelola_produk))
            .perform(click())

        onView(withText("BFR"))
            .perform(click())

        // Edit name and price
        onView(withHint(R.string.hint_product_name))
            .perform(replaceText("Big Falcon Rocket"))
        onView(withHint(R.string.hint_price))
            .perform(typeText("100"))
        onView(withText(R.string.label_save_changes))
            .perform(click())

        // Product edit succeeds
        onView(withText("Big Falcon Rocket"))
            .check(matches(isDisplayed()))
        onView(withSubstring("Rp1.000.100"))
            .check(matches(isDisplayed()))
    }

    @Test
    fun deleteExistingProduct() {
        runBlocking {
            repositoryComponent.productRepository.insert(Product(name = "BFR", price = 1000))
        }

        activityRule.launchActivity(null)

        onView(withId(R.id.admin_container))
            .perform(click())

        onView(withText(R.string.kelola_produk))
            .perform(click())

        // Delete product
        onView(withText("BFR"))
            .perform(click())
        onView(withText("DELETE"))
            .perform(click())

        // Make sure product is deleted
        onView(withText("BFR"))
            .check(doesNotExist())
    }

    @Test
    fun deleteCanBeUndone() {
        runBlocking {
            repositoryComponent.productRepository.insert(Product(name = "BFR", price = 1000))
        }

        activityRule.launchActivity(null)

        onView(withId(R.id.admin_container))
            .perform(click())

        onView(withText(R.string.kelola_produk))
            .perform(click())

        // Delete product
        onView(withText("BFR"))
            .perform(click())
        onView(withText("DELETE"))
            .perform(click())

        // Make sure product is deleted
        onView(withText("BFR"))
            .check(doesNotExist())

        // Undo deleteProduct
        onView(withText(R.string.undo))
            .perform(click())

        // Make sure product return when undo is clicked
        onView(withText("BFR"))
            .check(matches(isDisplayed()))
    }
}