package io.decapos.posix.e2e

import androidx.test.core.app.ApplicationProvider.getApplicationContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import io.decapos.posix.MainActivity
import io.decapos.posix.R
import io.decapos.posix.repository.dagger.DaggerRepositoryComponent
import okhttp3.logging.HttpLoggingInterceptor
import org.junit.Before
import org.junit.Ignore
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

// Test navigation to all fragments. Test is done on device with english locale
@RunWith(AndroidJUnit4::class)
@LargeTest
class NavigationTest {
    private val repositoryComponent = DaggerRepositoryComponent.builder().application(getApplicationContext())
        .networkInterceptor(HttpLoggingInterceptor {}).build()

    @get:Rule
    var activityRule = ActivityTestRule(MainActivity::class.java, false, false)

    // espresso doesn't know when dataBinding is idling, so we need to tell it when with this.
    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    @Before
    fun resetDatabase() {
        repositoryComponent.generalRepository.clearAllData()
    }

    @Test
    fun bottomNavigation() {
        activityRule.launchActivity(null)

        // should start on cashier
        onView(withText("No products yet, add your products first."))
            .check(matches(isDisplayed()))

        // navigate to orders
        onView(withId(R.id.order_history))
            .perform(click())
        onView(withText("You haven't made any order yet"))
            .check(matches(isDisplayed()))

        // navigate to report
        onView(withId(R.id.report_container))
            .perform(click())
        onView(withText("Sales data still empty, your selling skill sucks."))
            .check(matches(isDisplayed()))

        // navigate to admin
        onView(withId(R.id.admin_container))
            .perform(click())
        onView(withText(R.string.kelola_produk))
            .check(matches(isDisplayed()))

        // navigate to beta community
        onView(withId(R.id.community))
            .perform(click())
        onView(withText("Beta Community"))
            .check(matches(isDisplayed()))

        // navigate back to cashier
        onView(withId(R.id.cashier_container))
            .perform(click())
        onView(withText("No products yet, add your products first."))
            .check(matches(isDisplayed()))
    }

    @Test
    fun adminSectionNavigation() {
        activityRule.launchActivity(null)

        // navigate to admin section
        onView(withId(R.id.admin_container))
            .perform(click())

        // navigate to manage product
        onView(withText(R.string.kelola_produk))
            .perform(click())

        // go to add products
        onView(withId(R.id.btn_add_products))
            .perform(click())
        pressBack()
        onView(withId(R.id.btn_add_products))
            .perform(click())

        // go back to admin menu
        pressBack()
        pressBack()

        // go to printer
        onView(withText("Printer"))
            .perform(click())
        onView(withId(R.id.fab_add_printer))
            .perform(click())
        // TODO: test navigation to printer

        // back to admin menu
        pressBack()

        // go to settings
        onView(withText("Settings"))
        pressBack()
    }

    @Test
    @Ignore("Pending Implementation")
    fun betaCommunityNavigation() {
    }
}