package io.decapos.posix.test_shared

import io.decapos.posix.vo.Customer
import io.decapos.posix.vo.Order
import io.decapos.posix.vo.OrderItem
import io.decapos.posix.vo.Product
import org.threeten.bp.Instant

object TestUtils {
    fun createCustomer(name: String) = Customer(
        idCustomer = 1,
        name = name,
        phoneNumber = ""
    )

    fun createOrder() = Order(
        idOrder = 2,
        totalPayAmount = 1000,
        actualPayAmount = 1000
    )

    fun createOrders(count: Int, totalPayAmount: Long = 1000L, timeMultiplier: Long = 1L) = (0 until count).map {
        Order(
            totalPayAmount = totalPayAmount,
            actualPayAmount = totalPayAmount,
            createdAt = Instant.ofEpochSecond(it * timeMultiplier)
        )
    }

    fun createOrderItem(order: Order) = OrderItem(
        idOrderItem = 1,
        idOrder = order.idOrder,
        name = "Shakalaka boom boom",
        price = 1200
    )

    fun createProducts(count: Int) = (0 until count).map {
        Product(
            idProduct = it,
            name = it.toString(),
            price = 1000L * count
        )
    }
}