package io.decapos.posix.vo

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity
data class Product(
    @PrimaryKey(autoGenerate = true)
    val idProduct: Int? = null,
    var name: String = "",
    var price: Long = 0,
    var imagePath: String = "",
    var createdAt: Instant = Instant.now(),
    var updatedAt: Instant = Instant.now()
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readValue(Int::class.java.classLoader) as? Int,
        parcel.readString() ?: "",
        parcel.readLong(),
        parcel.readString() ?: "",
        Instant.parse(parcel.readString()),
        Instant.parse(parcel.readString())
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(idProduct)
        parcel.writeString(name)
        parcel.writeLong(price)
        parcel.writeString(imagePath)
        parcel.writeString(createdAt.toString())
        parcel.writeString(updatedAt.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Product> {
        override fun createFromParcel(parcel: Parcel): Product {
            return Product(parcel)
        }

        override fun newArray(size: Int): Array<Product?> {
            return arrayOfNulls(size)
        }
    }
}
