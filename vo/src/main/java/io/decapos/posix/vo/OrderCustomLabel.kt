package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(
    foreignKeys = [ForeignKey(
        entity = CustomLabelValue::class,
        parentColumns = ["idCustomLabelValue"],
        childColumns = ["idCustomLabelValue"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    ), ForeignKey(
        entity = Order::class,
        parentColumns = ["idOrder"],
        childColumns = ["idOrder"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("idCustomLabelValue"), Index("idOrder")]
)
data class OrderCustomLabel(
    @PrimaryKey(autoGenerate = true)
    val idOrderCustomLabel: Int? = null,
    val idCustomLabelValue: Int? = null,
    val idOrder: Int? = null,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)