package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(
    foreignKeys = [ForeignKey(
        entity = CustomLabelName::class,
        parentColumns = ["idCustomLabelName"],
        childColumns = ["idCustomLabelName"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("idCustomLabelName")]
)
data class CustomLabelValue(
    @PrimaryKey(autoGenerate = true)
    val idCustomLabelValue: Int? = null,
    val idCustomLabelName: Int? = null,
    val value: String,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)