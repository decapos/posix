package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(
    foreignKeys = [ForeignKey(
        entity = Customer::class,
        parentColumns = ["idCustomer"],
        childColumns = ["idCustomer"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_NULL
    ), ForeignKey(
        entity = OrderStatus::class,
        parentColumns = ["idStatus"],
        childColumns = ["idStatus"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.SET_NULL
    )],
    indices = [Index("idCustomer"), Index("idStatus")]
)
data class Order(
    @PrimaryKey(autoGenerate = true)
    val idOrder: Int? = null,
    val idCustomer: Int? = null,
    val idUser: Int? = null,
    val idStatus: Int? = null,
    val tax: Float = 0f,
    val totalPayAmount: Long, // Total price of product
    val actualPayAmount: Long, // Actual payment from customer
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)
