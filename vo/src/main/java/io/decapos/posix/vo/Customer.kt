package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity
data class Customer(
    @PrimaryKey(autoGenerate = true)
    val idCustomer: Int,
    val name: String,
    val phoneNumber: String,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)
