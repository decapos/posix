package io.decapos.posix.vo

import androidx.room.Embedded
import androidx.room.Relation

class ProductsWithCartItem {
    @Relation(
        parentColumn = "idProduct",
        entityColumn = "idProduct",
        entity = CartItem::class
    )
    lateinit var cartDetail: List<CartItem>

    @Embedded
    lateinit var product: Product
}