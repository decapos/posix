package io.decapos.posix.vo

import org.threeten.bp.YearMonth
import org.threeten.bp.ZoneId

class MonthlySummary(orders: List<Order>, yearMonth: YearMonth?) {
    val yearMonth: YearMonth = yearMonth ?: YearMonth.from(orders.first().createdAt.atZone(ZoneId.systemDefault()))
    val revenue = orders.fold(0L) { acc, order -> acc + order.totalPayAmount }
}
