package io.decapos.posix.vo

data class ProductTotalOrdered(val productName: String, val totalOrdered: Float)