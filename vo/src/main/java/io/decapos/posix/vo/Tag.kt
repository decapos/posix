package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity
data class Tag(
    @PrimaryKey(autoGenerate = true)
    val idTag: Int? = null,
    val value: String,
    var createdAt: Instant = Instant.now(),
    var updatedAt: Instant = Instant.now()
)