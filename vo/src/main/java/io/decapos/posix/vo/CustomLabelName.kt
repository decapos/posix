package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity
data class CustomLabelName(
    @PrimaryKey(autoGenerate = true)
    val idCustomLabelName: Int? = null,
    val name: String,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)