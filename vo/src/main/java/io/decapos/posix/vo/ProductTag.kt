package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(
    foreignKeys = [ForeignKey(
        entity = Tag::class,
        childColumns = ["idTag"],
        parentColumns = ["idTag"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    ), ForeignKey(
        entity = Product::class,
        childColumns = ["idProduct"],
        parentColumns = ["idProduct"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("idTag"), Index("idProduct")]
)
data class ProductTag(
    @PrimaryKey(autoGenerate = true)
    val idProductTag: Int? = null,
    val idTag: Int,
    val idProduct: Int,
    var createdAt: Instant = Instant.now(),
    var updatedAt: Instant = Instant.now()
)