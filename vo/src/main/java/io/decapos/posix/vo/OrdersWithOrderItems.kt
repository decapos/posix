package io.decapos.posix.vo

import androidx.room.Embedded
import androidx.room.Relation

data class OrdersWithOrderItems(
    @Embedded
    var order: Order,

    @Relation(
        parentColumn = "idOrder",
        entityColumn = "idOrder",
        entity = OrderItem::class
    )
    var orderItem: List<OrderItem>
)