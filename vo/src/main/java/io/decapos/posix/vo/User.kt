package io.decapos.posix.vo

import androidx.room.Entity
import org.threeten.bp.Instant

@Entity(primaryKeys = ["idUser"])
data class User(
    val idUser: Int,
    val name: String,
    val role: Int,
    val pin: String,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
)
