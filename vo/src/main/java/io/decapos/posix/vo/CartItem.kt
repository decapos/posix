package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = Product::class,
        parentColumns = ["idProduct"],
        childColumns = ["idProduct"],
        onUpdate = ForeignKey.CASCADE,
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index("idProduct")]
)
data class CartItem(
    @PrimaryKey(autoGenerate = true)
    val idCartItem: Int?,
    val idProduct: Int?,
    val quantity: Int
)
