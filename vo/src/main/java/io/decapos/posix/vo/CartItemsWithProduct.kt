package io.decapos.posix.vo

import androidx.room.Embedded
import androidx.room.Relation

class CartItemsWithProduct {
    @Embedded
    lateinit var cartDetail: CartItem

    @Relation(
        parentColumn = "idProduct",
        entityColumn = "idProduct",
        entity = Product::class
    )
    lateinit var product: List<Product>
}
