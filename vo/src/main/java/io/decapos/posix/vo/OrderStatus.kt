package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class OrderStatus(
    @PrimaryKey(autoGenerate = true)
    val idStatus: Int? = null,
    val name: Int
)