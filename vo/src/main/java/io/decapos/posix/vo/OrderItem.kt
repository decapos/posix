package io.decapos.posix.vo

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey
import org.threeten.bp.Instant

@Entity(
    foreignKeys = [ForeignKey(
        entity = Order::class,
        parentColumns = ["idOrder"],
        childColumns = ["idOrder"],
        onDelete = ForeignKey.CASCADE,
        onUpdate = ForeignKey.CASCADE
    )],
    indices = [Index("idOrder")]
)
data class OrderItem(
    @PrimaryKey(autoGenerate = true)
    val idOrderItem: Int? = null,
    var idOrder: Int? = null,
    val name: String,
    var quantity: Int = 1,
    val price: Long,
    val createdAt: Instant = Instant.now(),
    val updatedAt: Instant = Instant.now()
) {
    val totalPrice: Long
        get() = quantity * price

    fun setQuantity(input: CharSequence) {
        val qtyString = input.toString()
        quantity = if (qtyString == "" || qtyString == "0" || qtyString == "00") 1 else qtyString.toInt()
    }
}
